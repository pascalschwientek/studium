package de.plsk.factorysimulation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Stock {
    private ArrayList<Product> products;

    /**
     * Initialize a new Stock
     */
    public Stock() {
        products = new ArrayList<>();
    }

    /**
     * add a product to store
     *
     * @param product product to add
     */
    public void add(Product product) {
        products.add(product);
    }

    /**
     * count products in store
     *
     * @param name name of product
     * @return count
     */
    public int count(String name) {

        List<Product> results = new ArrayList<Product>();
        for (Product product : products) {
            if (product.getName().matches(name)) {
                results.add(product);
            }
        }

        return results.size();
    }

    /**
     * remove a product from the store
     *
     * @param name the product name to remove
     */
    public void remove(String name) {
        boolean removed = false;

        Iterator<Product> iter = products.iterator();

        while (iter.hasNext() && !removed) {
            Product product = iter.next();

            if (product.getName().matches(name)){
                iter.remove();
                removed = true;
            }
        }
    }

    /**
     * remove multiple products form the store
     *
     * @param name the product name to remove
     * @param count the amount of products to remove
     */
    public void remove(String name, int count) {
        int removed = 0;

        Iterator<Product> iter = products.iterator();

        while (iter.hasNext() && removed < count) {
            Product product = iter.next();

            if (product.getName().matches(name)){
                iter.remove();
                removed++;
            }
        }
    }

    /**
     * sell all products currently in the store
     *
     * @return the total sell value of all products
     */
    public double sell() {
        double value = .0;

        for (Product product : products) {
            value += product.getValue();
        }

        products.clear();

        return value;
    }

}
