#include <stdio.h>

struct Bruch {
    unsigned int zaehler;
    unsigned int nenner;
};

unsigned int ggt(struct Bruch bruch)
{
    if (bruch.zaehler == 0) {return bruch.nenner;}

    while(bruch.nenner != 0){
    	if (bruch.zaehler > bruch.nenner){
    		bruch.zaehler = bruch.zaehler - bruch.nenner;
    	}else{
    		bruch.nenner = bruch.nenner - bruch.zaehler;
    	}
    }

    return bruch.zaehler;
}

int main(void)
{
    unsigned int a, b, factor;
    struct Bruch bruch;

    printf("Programm zum Kürzen von Brüchen\n");

    printf("\nBitte geben Sie den Zähler ein: ");
    scanf("%d", &bruch.zaehler);

    printf("Bitte geben Sie den Nenner ein: ");
    scanf("%d", &bruch.nenner);

    if (bruch.zaehler < bruch.nenner) {
        printf("Zwei positive Zahlen a und b mit a >= b nicht gegeben");
        return 1;
    }

    factor = ggt(bruch);

    printf("\n---\n");
    printf("Der größte gemeinsame Teiler ist: %d\n", bruch.nenner/factor);
    printf("Der gekürzte Bruch heißt: %d/%d\n", bruch.zaehler/factor, bruch.nenner/factor);

    return 0;
}
