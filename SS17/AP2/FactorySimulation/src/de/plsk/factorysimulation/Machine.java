package de.plsk.factorysimulation;

public class Machine {
    // Protected would make sense
    private String name;
    private double cost;
    private Factory factory = null;
    protected String type;

    /**
     * @param name the name of the machine
     * @param cost ??
     */
    public Machine(String name, Double cost) {
        this.name = name;
        this.cost = cost;
        this.type = "Machine";
    }

    /**
     * @return the name of the machine
     */
    public String getName() {
        return name;
    }

    /**
     * TODO javadoc
     */
    public String getType() {
        return type;
    }

    /**
     * @return the cost of the machine
     */
    public double getCost() {
        return cost;
    }

    public Factory getFactory() {
        return factory;
    }

    /**
     * @param f the factory this machine is in
     */
    public void setFactory(Factory f) {
        factory = f;
    }

    /**
     * Start a machine
     */
    public void startMachine() {
        System.out.printf("%s has started.%n", getName());
    }

    /**
     * TODO javadoc
     */
    public Product getProducing() {
        return null;
    }

    /**
     * TODO javadoc
     */
    public Product getDependency() {
        return null;
    }

    /**
     * TODO javadoc
     */
    public int getDependencyCount() {
        return 0;
    }
}
