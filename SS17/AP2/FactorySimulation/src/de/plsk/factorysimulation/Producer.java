package de.plsk.factorysimulation;

public class Producer extends Machine {
    private Product producing;

    /**
     * Initialize a new Producer
     *
     * @param name The name of this Producer / Machine
     * @param cost The cost of this Machine
     * @param producing The Product this Producer / Machine is making
     */
    public Producer(String name, Double cost, Product producing) {
        super(name, cost);
        setProducing(producing);

        this.type = "Producer";
    }

    /**
     * Start a Machine
     */
    @Override
    public void startMachine() {
        super.startMachine();
        produce();
    }

    /**
     * TODO: javadoc
     */
    protected void produce() {
        Factory factory = getFactory();

        if(factory.getTestcredit() >= getCost()) {
            //factory.reduceTestcredit(getCost());

            factory.getStock().add(getProducing());

            System.out.printf("%s produced.%n", getProducing().getName());
        } else {
            System.out.printf("%s could not be produced. Incipient credit%n", getProducing().getName());
        }
    }

    /**
     * @return The Product this Producer is making
     */
    @Override
    public Product getProducing() {
        return producing;
    }

    /**
     * @param p The Product this Producer is making
     */
    private void setProducing(Product p) {
        producing = p;
    }
}
