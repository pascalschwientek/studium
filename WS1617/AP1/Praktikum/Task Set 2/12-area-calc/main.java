import java.util.Scanner;

public class main
{
    private static Scanner in;

    public static void main(String[] args)
    {
        int seitea;
        int seiteb;
        double umfang;
        double flaeche;

        System.out.println("Programm zur Berechnung des Umfanges und der Fläche von Rechtecken. \n");

        System.out.println("Bitte geben Sie die Seite a in cm ein: ");
        Scanner a = new Scanner(System.in);
        seitea = a.nextInt();
        a.close();

        System.out.println("\r\n");
        System.out.println("Bitte geben Sie die Seite b in cm ein: ");
        Scanner b = new Scanner(System.in);
        seiteb = b.nextInt();
        b.close();



        System.out.println("Die Seite a ist "+ seitea +" cm lang. \n");
        System.out.println("Die Seite b ist "+ seiteb +"  cm lang. \n");

        umfang = 2 * seitea + 2 * seiteb;
        System.out.println("Der Umfang des Rechteckes beträgt "+ umfang +"  cm. \n");

        flaeche = seitea * seiteb;
        System.out.println("Die Flaeche des Rechteckes betraegt "+ flaeche +"  cm. \n");
    }
}