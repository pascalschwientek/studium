package de.plsk.factorysimulation;

//TODO: why is this inheriting from Producer and not from Machine?
public class Processor extends Producer {
    private Product dependency;
    private int dependencyCount;

    /**
     * TODO javadoc
     *
     * @param name
     * @param cost
     */
    public Processor(String name, Double cost, Product producing, Product dependency, int dependencyCount) {
        super(name, cost, producing);

        this.dependency = dependency;
        this.dependencyCount = dependencyCount;
    }

    /**
     * @return Dose the factory have sufficient credit funds?
     */
    private boolean sufficientDependencies() {
        return getFactory().getStock().count(dependency.getName()) >= dependencyCount;
    }

    /**
     * TODO javadoc
     */
    @Override
    protected void produce() {
        if(sufficientDependencies()) {
            super.produce();

            getFactory().getStock().remove(dependency.getName(), dependencyCount);
        }
    }
}
