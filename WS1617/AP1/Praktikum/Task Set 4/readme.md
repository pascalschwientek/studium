# Übungsblatt 4

## Aufgabe 27

Schreiben Sie ein Java-Programm das per Benutzereingabe ganzzahlige Werte in ein Array speichert. 
Anschließend sollen die Werte aus dem Array gelesen und formatiert ausgegeben werden. 
Bitte benutzen Sie für die Ein- sowie die Ausgabe jeweils eine for-each-Schleife.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm Array Ein- und Ausgabe

Bitte geben Sie den 1. Wert ein: ___
Bitte geben Sie den 2. Wert ein: ___
Bitte geben Sie den 3. Wert ein: ___
Bitte geben Sie den 4. Wert ein: ___
Bitte geben Sie den 5. Wert ein: ___

Sie haben folgende Werte eingegeben: x, x, …, x
```

[Source Code](27-build-array/main.java)

## Aufgabe 28

Schreiben Sie ein Java-Programm, das den Median von sechs Werten bestimmt. 
Die Werte sollen per Benutzereingabe in einem Array gespeichert werden. 
Anschließend soll der Median der Werte bestimmt und dieser auf der Konsole ausgegeben werden.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm Median

Bitte geben Sie den 1. Wert ein: ___
Bitte geben Sie den 2. Wert ein: ___
Bitte geben Sie den 3. Wert ein: ___
Bitte geben Sie den 4. Wert ein: ___
Bitte geben Sie den 5. Wert ein: ___
Bitte geben Sie den 6. Wert ein: ___

Der Median der eingegebenen Zahlenfolge ist: xx
```

[Source Code](28-median/main.java)

**Hinweis 1:** Diese Aufgabe ist nicht so ganz einfach. Es wird hier der Median bestimmt 
und nicht das arithmetische Mittel. Sie müssen sich also zunächst überlegen, wie Sie die 
Zahlen der Größe nach im Array sortieren. Sie können dazu ein einfaches Verfahren Ihrer 
Wahl verwenden. Informieren Sie sich in der Literatur über einfache Sortieralgorithmen.

**Hinweis 2:** Verwenden Sie for-each-Schleifen.

## Aufgabe 29

Schreiben Sie ein Java-Programm, das die Primzahlen zwischen 2 bis maximal 1000 berechnet 
und zwar nach der altehrwürdigen Methode „Sieb des Eratosthenes&quot; (3. Jahrhundert. v. Chr.): 
Das Verfahren berechnet für eine eingegebene natürliche Zahl (&gt;1) alle Primzahlen bis 
einschließlich dieser Zahl. Für falsche oder gar keine Eingaben geben Sie bitte eine 
entsprechende Fehlermeldung aus.

**Hinweis:** Verwenden Sie ein boolean-Array, dessen Komponenten trueoder false sein können. 
Das Ziel besteht darin, dem i-ten Element den Wert true zu zuweisen, falls i eine Primzahl ist, 
und andernfalls den Wert false. Dies wird erreicht, indem für jedes ialle Elemente des Arrays, 
die einem beliebigen Vielfachen von i entsprechen, auf false gesetzt werden. Danach wird das Feld 
nochmals durchlaufen, um alle gefundenen Primzahlen auszugeben.

[Source Code](29-prime-calc/main.java)

## Aufgabe 30

Schreiben sie ein Java-Programm, das ermittelt, ob es sich bei einer eingegebenen Jahreszahl um ein Schaltjahr handelt.

**Hilfestellung:** Ist die Jahreszahl durch 4 teilbar, aber nicht durch 100, so ist es ein Schaltjahr. 
Ist die Jahreszahl durch 100 teilbar, aber nicht durch 400, so ist es kein Schaltjahr. Ist die Jahreszahl 
durch 400 teilbar, dann ist es ein Schaltjahr.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm Schaltjahr

Bitte geben Sie eine Jahreszahl ein: 1993

1993 ist kein Schaltjahr!
```

Geben Sie natürlich auch aus, wenn es sich um kein Schaltjahr handelt.

[Source Code](30-is-leap-year/main.java)

## Aufgabe 31

Schreiben Sie ein Java-Programm, das eine 5*5 Matrix voller Nullen erstellt und diese ausgibt. 
Anschließend sollen dem Benutzer zwei Auswahlmöglichkeiten angeboten werden, wie die Matrix gefüllt werden soll:

Option 1: Fülle die erste Zeile und letzte Spalte mit Einsen

Option 2: Fülle die beiden Diagonalen der Matrix mit Einsen

Nach der Auswahl soll die Matrix, wie vom Nutzer ausgewählt, gefüllt und ausgegeben werden.

**Hinweis:** Nutzen Sie zum Füllen der Matrix unbedingt Schleifen. Dies ist aus Übungszwecken eine wichtige 
Anforderung dieser Aufgabe.

[Source Code](31-matrix-generator/main.java)

## Aufgabe 32

Schreiben Sie ein Java-Programm, das eine eingegebene positive ganze Zahl inklusive 
der 0 in eine 8-Bit Dualzahl umwandelt. Geben Sie eine Fehlermeldung aus, falls 
die eingegebene Zahl zu groß ist.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm Dualzahl

Geben Sie bitte eine ganze Zahl ein: 3

Ausgabe im Dualsystem: 00000011
```

[Source Code](32-8-bit-converter/main.java)

## Aufgabe 33

Schreiben Sie ein Java-Programm, das zwei Integerzahlen (Typ int) einliest und addiert.

```
Programm Bereichsüberschreitungen

Geben Sie bitte zwei Integerzahlen ein: x, y

Die Summe von x und y ist: _______
```

Experimentieren Sie mit sehr großen Werten und erläutern Sie in 5 Sätzen, welche Effekte auftreten und woran das liegt.

[Source Code](33-int-sum/main.java)

**Antwort:** 


## Aufgabe 34

Beantworten Sie die folgenden Fragen schriftlich:

### Wozu dient import in Java?

Um Pakete in Java zu nutzen die nicht teil vom Java Core sind.

### Wieso ist es guter Java-Stil, Deklarationen und Anweisungen zu mischen? Man deklariert beispielsweise Variable erst an der Stelle, wo man sie zum ersten Mal benutzt (und nicht wie in C am Anfang eines Blocks). Machen Sie das an folgenden Beispiel klar:
    int x, y;
    <weitere Anweisungen>
    int z = x + y;
    
Unter Umständen gibt es schon vor der weiteren Deklaration einen Return oder anderweitigen Sprung im Code. Dann muss die variable gar nicht erst 
im Speicher deklariert werden.

### Was versteht man unter Lokalisierung und mit welcher Anweisung stellt man die Lokalisierung auf eine andere Sprache um.

Lokalisierung ist die Übersetzung von Programmen in andere Sprachen.

    Locale.setDefault(Locale)

### Ab Java 5 darf man das aus C bekannte printf benutzen. Für UNIX-Systeme kann man den Zeilenwechsel mit \n programmieren. Recherchieren Sie, ob dies für alle Plattformen, z.B. Windows, funktioniert. Welche Empfehlung geben Java-Spezialisten für Applikationen auf Cross Plattformen?

Windows verwendet \r für ein newline.

    String newLine = System.getProperty("line.separator");

### Geben Sie für die Anweisung ```int[] zahlen = new int[5];``` zwei äquivalente Anweisungen an.

    int[] zahlen; zahlen = new int[5];
    
    int[5] zahlen;

### Java-Arrays sind Objekte, die ihre Länge kennen. Wie kann man auf die Länge eines Arrays zugreifen? Geben Sie bitte ein Beispiel an, wo dies in einer for-Schleife geschieht.

```java
for (int i = 0; i < array.length; i++) {
    System.out.print(refArray[i] + ", ");
}
```

### Erläutern Sie bitte Folgendes anhand einer kleinen Grafik, in der Sie die Objektreferenzen und die Objekte darstellen
```
int[] a = new int[5];
int[] b;
b = a;
```

#### Diskutieren Sie das Ergebnis von `equals(b));` in diesem Zusammenhang und geben Sie kurz an, was in der Java API zu equals steht.

Da b kein Objekt ist und sich equals auf kein Objekt bezieht kann es kein Ergebnis geben.

> Indicates whether some other object is "equal to" this one.

### Schreiben Sie ein kleines Java-Programm, das eine IndexOutOfBoundsException erzeugt, wenn auf ein Array-Element außerhalb der Array-Grenzen zugegriffen wird.
```java
public static void main(String[] args) {
    int[] array = new int[10];
    
    array[1337];
}
```

### Erläutern Sie die Vorteile, die sich ergeben, wenn man die Deklaration von Laufvariablen in for-Schleifen vornehmen kann: for(int i=1; i < 100; ++i) ...

So können die schleifen in sich geschlossen sein und erscheinen übersichtlicher. Die deklaration für die schleife selber muss nicht über der schleife im restliche code passieren.

### Schreiben Sie 5 Aussagen auf, die begründen, dass der Kauf von Java durch den amerikanischen Software-Giganten Oracle eine sinnvolle Investition war.

- Sun Microsystems hat schon lange Verluste gemacht und konnte die Zukunft von Java und anderen open source Produkten nicht sichern.
- Oracle konnte die Weiterentwicklung  sicheren.
- So konnte verhindert werden das Java ein Produkt von IBM wird.
- ...
- ...
