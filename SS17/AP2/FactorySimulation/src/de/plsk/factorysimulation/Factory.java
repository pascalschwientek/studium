package de.plsk.factorysimulation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.NoSuchElementException;

public class Factory {
    private Stock stock;
    private double credit;
    private double testcredit;
    private String name;
    private MachineList machines;
    private int discount;
    private boolean saleActive = false;

    /**
     * Create a new Factory
     */
    public Factory(String name, double testcredit) {
        this.name = name;
        this.machines = new MachineList();
        this.stock = new Stock();
        this.testcredit = testcredit;
        this.credit = testcredit;
    }

    /**
     * Create a new Factory
     */
    public Factory(String name, double testcredit, int discount) {
        this.name = name;
        this.machines = new MachineList();
        this.stock = new Stock();
        this.testcredit = testcredit;

        if(discount > 0){
            this.discount = discount;
            this.saleActive = true;
        }
    }

    /**
     * TODO javadoc
     *
     * @return
     */
    public double getTestcredit() {
        return testcredit;
    }

    /**
     * TODO javadoc
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * TODO javadoc
     *
     * @return
     */
    public Stock getStock() {
        return stock;
    }

    /**
     * TODO javadoc
     *
     * @param machine
     */
    public void addMachine(Machine machine) {

        testcredit = testcredit - machine.getCost();

        machine.setFactory(this);

        this.machines.addLast(machine);
    }

    /**
     * TODO javadoc
     *
     * @return
     */
    public MachineList getMachines() {
        return machines;
    }

    /**
     * TODO javadoc
     */
    public void resetFactory() {
        testcredit = credit;
        machines.clear();
    }

    /**
     * TODO javadoc
     *
     * @param index
     */
    public void removeMachine(int index) {
        Machine machine;

        try {
            machine = machines.get(index);
        } catch (NoSuchElementException e) {
            System.out.printf("Can't remove machine. No such element with index %d", index);
            return;
        } catch (IndexOutOfBoundsException e) {
            System.out.printf("Index of %d too big ot too small", index);
            return;
        }

        testcredit = testcredit + machine.getCost();

        machines.remove(index);
    }

    public void sort(Comparator comp) {
        machines.sort(comp);
    }

    /**
     * TODO javadoc
     *
     * @param cost
     */
    public void reduceTestcredit(double cost) {
        this.testcredit -= cost;
    }

    public void factorySale(int discount)
    {
        ArrayList<Product> products = stock.getProducts();
        Stock discountedStock = new Stock();

        for (Product product : products) {
            Double newValue = product.getValue()*(1.0-(discount / 100.0));
            discountedStock.add(new Product(product.getName(), product.getCost(), newValue));

            System.out.printf("%s was %f is now %f\n", product.getName() ,product.getValue(), newValue);
        }

        stock = discountedStock;
    }

    /**
     * TODO javadoc
     *
     * @param rounds
     */
    public double simulate(int rounds) {

        while (rounds > 0) {
            for (Machine machine : machines) {
                machine.startMachine();

            }

            if(saleActive) {
                factorySale(discount);
            }

            testcredit += stock.sell();
            rounds--;
        }

        return testcredit;
    }
}
