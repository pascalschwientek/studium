#include <stdio.h>

int main(void)
{
    float a, b;

    printf("Programm zur Berechnung des Umfanges und der Fläche von Rechtecken\n");

    printf("\nBitte geben Sie die Seite a in cm ein: ");
    scanf("%f", &a);

    printf("\nBitte geben Sie die Seite b in cm ein: ");
    scanf("%f", &b);

    printf("Die Seite a ist %08.2f cm lang.\n", a);
    printf("Die Seite b ist %08.2f cm lang.\n", b);

    printf("Der Umfang des Rechtecks beträgt %08.2f cm.\n", a+b);

    printf("Die Fäche des Rechtecks beträgt %08.2f cm2.\n", a*b);
}
