import java.util.Scanner;

public class main
{
    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] matrix = new int[5][5];
        int option;

        for(int row = 0; row < 5; row++) {
            for(int column = 0; column < 5; column++) {
                matrix[row][column] = 0;
                System.out.print(0);
            }
            System.out.println();
        }

        System.out.println("Wie soll die Matrix gefüllt werden?");
        System.out.println("Option 1: Fülle die erste Zeile und letzte Spalte mit Einsen");
        System.out.println("Option 2: Fülle die beiden Diagonalen der Matrix mit Einsen");
        option = STDIN_SCANNER.nextInt();

        switch(option) {
            case 1:
                for(int column = 0; column < 5; column++) {
                    matrix[1][column] = 1;
                    matrix[5][column] = 1;
                }
                break;
            case 2:
                for(int row = 0; row < 5; row++) {
                    matrix[row][row] = 1;
                    matrix[row][4 - row] = 1;
                }
                break;
        }

        for(int row = 0; row < 5; row++) {
            for(int column = 0; column < 5; column++) {
                System.out.print(matrix[row][column]);
            }
            System.out.println();
        }
    }
}