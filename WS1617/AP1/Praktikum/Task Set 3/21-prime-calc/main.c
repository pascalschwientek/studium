#include <stdio.h>
#include <math.h>

#define N_LIMIT 1000

void sieb(int n, int primes[])
{
    int i, j;
    for (i=0;i<n;i++){
        primes[i] = 1;
    }

    primes[0]=0,primes[1]=0; // 0,1 keine prime

    for (i=2;i<sqrt(n);i++){
        for (j=i*i;j<n;j+=i){
            primes[j] = 0;
        }
    }
}

int main(void)
{
    int n, i;

    printf("Sieb des Eratosthenes\n");

    printf("Primzahlen berechnen bis: ");
    scanf("%d", &n);

    if(n > N_LIMIT || n < 2)
    {
        printf("Primzahl bereich zu klein / groß!");
        return 1;
    }

    int numbers[n];

    for(i=0; i<=n; i++) {
    	numbers[i] = 0;
    }

    sieb(n, numbers);

    for(i=0; i<n; i++) {
        if (numbers[i] == 1){
            printf("%d, ", i);
        }
    }

    return 0;
}
