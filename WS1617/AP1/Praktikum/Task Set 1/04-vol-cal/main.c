#include <stdio.h>
#include <math.h>

int main(void)
{
    float radius, volume, area;

    printf("Programm Kugelberechnung\n");

    printf("\nBitte geben Sie den Radius der Kugel in cm ein: ");
    scanf("%f", &radius);

    volume = (4*M_PI*powf(radius,3))/3;
    area = 4*M_PI*powf(radius,2);

    printf("Das Volumen für die Kugel ist %6.2f cm3.\n", volume);
    printf("Die Oberfläche beträgt %6.2f cm2.\n", area);

    return 0;
}
