import java.util.Scanner;

public class main
{
    public final static int N_LIMIT = 1_000;

    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static void sieb(int n, int[] primes) {
        for(int i = 0; i < n; i++) {
            primes[i] = 1;
        }

        primes[0] = 0;
        primes[1] = 0; // 0,1 keine prime

        for(int i = 2; i < Math.sqrt(n); i++) {
            for(int j = i * i; j < n; j += i) {
                primes[j] = 0;
            }
        }
    }

    public static void main(String[] args) {
        int n;

        System.out.println("Sieb des Eratosthenes");

        System.out.print("Primzahlen berechnen bis: ");
        n = STDIN_SCANNER.nextInt();

        if(n > N_LIMIT || n < 2) {
            System.out.print("Primzahl bereich zu klein / groß!");
            System.exit(1);
            return;
        }

        int[] numbers = new int[N_LIMIT];

        for(int i = 0; i <= n; i++) {
            numbers[i] = 0;
        }

        sieb(n, numbers);

        for(int i = 0; i < n; i++) {
            if(numbers[i] == 1) {
                System.out.print(i + ", ");
            }
        }
    }
}