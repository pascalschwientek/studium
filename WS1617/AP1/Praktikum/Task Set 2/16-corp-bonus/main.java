import java.util.Scanner;

public class main
{
    private static Scanner in;

    public static void main(String[] args)
    {
        int alter;
        int dienstalter;
        double bonus;
        bonus = 0;

        System.out.println("Alter: ");
        Scanner a = new Scanner(System.in);
        alter = a.nextInt();
        a.close();

        System.out.println("Dienstalter: ");
        Scanner b = new Scanner(System.in);
        dienstalter = b.nextInt();
        b.close();

        if(dienstalter < 1) {
            System.out.println("600 EUR Praemie.\n");
        }
        else {
            if(dienstalter >= 1 && dienstalter < 6) {
                System.out.println("600 EUR Praemie.\n");
                bonus = 600;
            }
            if(dienstalter >= 6) {
                System.out.println("250 EUR Praemie und 75 EUR fuer jedes Dienstjahr. \n");
                bonus = 250 + (75 * dienstalter);
            }
            if(alter > 50) {
                System.out.println("250 EUR Zulage. \n");
                bonus = bonus + 250;
            }
            System.out.println("Die Jahrespraemie betraegt: "+ bonus +" EUR");
        }
    }
}