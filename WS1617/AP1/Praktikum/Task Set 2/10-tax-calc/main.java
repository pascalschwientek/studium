import java.util.Scanner;

public class main
{
    private static Scanner in;

    public static void main(String[] args)
    {
        int nettobetrag;
        double mehrwertsteuersatz;
        double bruttobetrag;

        System.out.println("Programm zur Berechnung eines Bruttobetrages.\n");

        System.out.println("Bitte geben Sie den Nettobetrag in EUR ein: ");
        Scanner in = new Scanner(System.in);
        nettobetrag = in.nextInt();
        in.close();
        System.out.println("\n");

        mehrwertsteuersatz = nettobetrag * 0.19;
        bruttobetrag = mehrwertsteuersatz + nettobetrag;

        System.out.println("Nettobetrag = "+ nettobetrag +"   EUR \n");
        System.out.println("Mehrwertsteuersatz = "+ mehrwertsteuersatz +"  EUR \n");
        System.out.println("Bruttobetrag = "+ bruttobetrag +" EUR  \n");
    }
}
