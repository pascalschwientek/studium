#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int a, b;

    printf("Programm zur Berechnung von Summe, Differenz, Produkt und Quotient\n");

    printf("Zwei ganze Zahlen eingeben: ");
    scanf("%d %d", &a, &b);

    printf("Summe: %d\n", a+b);
    printf("Differenz: %d\n", abs(a-b));
    printf("Produkt: %d\n",  a*b);
    printf("Quotient: %d\n",  a/b);

    // Quotient lässt sich nicht immer als int abbielden.

    return 0;
}
