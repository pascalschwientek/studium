package de.plsk.factorysimulation;

import java.util.ArrayList;

public class Factory {
    private Stock stock;
    private double credit;
    public double testcredit;
    private String name;
    private ArrayList<Machine> machines;

    /**
     * Create a new Factory
     */
    public Factory(String name) {
        this.name = name;
        this.machines = new ArrayList<>();
        this.stock = new Stock();
    }

    /**
     * TODO javadoc
     * TODO: for what testcredit?
     *
     * @return
     */
    public double getTestcredit() {
        return testcredit;
    }

    /**
     * TODO javadoc
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * TODO javadoc
     *
     * @return
     */
    public Stock getStock() {
        return stock;
    }

    /**
     * TODO javadoc
     *
     * @param machine
     */
    public void addMachine(Machine machine) {
        machine.setFactory(this);

        this.machines.add(machine);
    }

    /**
     * TODO javadoc
     *
     * @param index
     */
    public void removeMachine(int index) {
        this.machines.remove(index);
    }

    /**
     * TODO javadoc
     *
     * @param cost
     */
    public void reduceTestcredit(double cost) {
        this.testcredit -= cost;
    }

    /**
     * TODO javadoc
     *
     * @param rounds
     */
    public double simulate(int rounds) {
        this.credit = this.testcredit;

        double cost = .0;

        for (Machine machine : this.machines) {
            cost += machine.getCost();
        }

        this.credit -= cost;

        while (rounds > 0) {
            for (Machine machine : machines) {
                machine.startMachine();

            }
            testcredit += stock.sell();
            rounds--;
        }

        return testcredit;
    }
}
