import java.util.Scanner;

public class main
{
    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static void main(String[] args)
    {
        int[] array = new int[5];

        System.out.println("Programm Array Ein- und Ausgabe");

        for(int i = 0; i < 5; i++) {
            System.out.print("Bitte geben Sie den " + (i + 1) + ". Wert ein: ");
            array[i] = STDIN_SCANNER.nextInt();
        }

        System.out.print("Sie haben folgende Werte eingegeben: ");

        for(int i = 0; i < 5; i++) {
            System.out.print(array[i]);
            if(i <= 3) {
                System.out.print(",");
            } else {
                System.out.println();
            }
        }
    }
}