## Übungsblatt 6

## Aufgabe 45

Schreiben Sie ein Java-Programm, das für ein Geburtsjahr, das eingegeben wird, das Alter einer Person berechnet.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm zur Berechnung eines Alters einer Person

Bitte geben Sie Ihr Geburtsjahr ein: ___________

Sie sind heute ______ Jahre alt.
```

Implementieren Sie dieses Programm mit den folgenden Funktionen:

1. ```public static int eingabeGebJahr()```

    Hier wird die Person nach dem Geburtsjahr gefragt. Dieses wird als Returnwert zurückgegeben.

1. ```public static int alterBerechnung(int gebJahr)```

    Das Geburtsjahr wird als Parameter übergeben, das Alter wird berechnet und als Returnwert zurückgegeben.

1. ```public static void ausgabeAlter(int alter)```

    Das Alter wird als Parameter übergeben und die obige Ausgabe wird erzeugt.

1. ```public static void main(String[] args)```

    Hier werden die obigen Methoden aufgerufen.
    
[Source Code](45-birthday-to-age/main.java)

## Aufgabe 46

Schreiben Sie ein Java-Programm, das ```x = y``` berechnet, wenn die Zahlen ```x``` und ```y``` eingegeben werden. 
Verwenden Sie hierzu die Methode ```pow``` der Java Klasse ```Math```.

**Hinweise (siehe auch Aufgabe 50, Frage 1):**

- Sie müssen sich in der Java API die Methode ```pow``` ansehen, um zu sehen, wie sie aufgerufen wird.
- Sie sollten sich Sie die Klasse ```Math``` ansehen, um zu verstehen, wie man Methoden dieser Klasse in eigenen Programmen aufrufen kann.
- Wenn Sie ohne ```import``` arbeiten wollen, dann funktioniert das mit ```Math.pow(x,y).```
- Wenn Sie ```import``` verwenden möchten, damit der Aufruf nur noch ```pow(x,y)``` heißt, versuchen Sie es mit ```import static java.lang.Math.*;```

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm zur Berechnung der Potenz

Bitte geben Sie x ein: ___________
Bitte geben Sie y ein: ___________

Die Potenz von x hoch y ist: ______
```

[Source Code](46-pow-calc/main.java)

## Aufgabe 47

Schreiben Sie ein Programm zur Ermittlung des größten gemeinsamen Teilers von zwei **positiven** Zahlen 
und zum Kürzen von Brüchen. Entwickeln Sie dazu als erstes die Methode

    ```public static int ggt(int a, int b)```

Der größte gemeinsame Teiler ist nach dem Euklidischen Algorithmus zu berechnen. Dieser geht davon aus, 
dass für zwei **positive** Zahlen **a** und **b** mit ```a >= b``` gilt:

    ```ggt(a,b) = ggt(b, a % b)```

Entwickeln Sie eine Schleife, die nach der angegebenen Grundidee die beiden Eingabegrößen **a** und **b** solange 
verkleinert, bis **b == 0** ist. In dem Fall ist **a** nämlich gleich dem gesuchten Resultat.

Die Methode **public static void main(String[] args)**soll nach Zähler und Nenner fragen und dann die 
```ggt```-Methode aufrufen, die den größten gemeinsamen Teiler berechnet. Dann soll der gekürzte Bruch ausgegeben werden.

Achten Sie darauf, dass die **main** -Methode dafür sorgt, dass Zähler und Nenner hinsichtlich ihrer Vorzeichen 
ordentlich behandelt werden, denn der ```ggt``` -Aufruf funktioniert nur für positive Zahlen.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm zum Kürzen von Brüchen

Bitte geben Sie den Zähler ein:         _________
Bitte geben Sie den Nenner ein:         _________

Der größte gemeinsame Teiler ist:       _________
        Der gekürzte Bruch heißt:       _________
```

**Hinweis:** An dieser Stelle ist **nicht** verlangt, dass Sie ein Bruchobjekt erzeugen. 
Das haben wir in der Vorlesung noch nicht besprochen. Später werden wir eine eigene Klasse für Brüche entwickeln, 
so dass Sie die Vorteile der Objektorientierung kennenlernen werden. Dies ist an dieser Stelle aus didaktischen 
Gründen noch nicht erwünscht. Sie sollen hier nur lernen, mit Methoden in Java umzugehen. Schreiben Sie also 
eine (!) Klasse mit den in der Aufgabe geforderten zwei Methoden, so wie Sie es kennen.

[Source Code](47-shared-divider/main.java)

## Aufgabe 48

Schreiben Sie ein Programm, das alles Mögliche mit ```integer``` -Arrays macht. Formulieren Sie alle auszuführenden Aktionen als Methoden.

Die ```main``` -Methode soll folgende Aktionen nacheinander ausführen:

- Einlesen von Zahlen mit der Methode ```public static int[] einlesen()```
    Hierbei soll gefragt werden, wie viele Zahlen einzulesen sind. Dann ist das Array anzulegen und außerdem soll 
    das Array als ```return``` -Wert zurückgegeben werden.
- Die Zahlen des Arrays in der Methode ```public static void ausgeben(int[] refArray)```
- Den Mittelwert der Zahlen in der Methode ```public static double mittelwert(int[] refArray)``` berechnen und als ```return``` -Wert zurückgeben.
- Die größte Zahl in der Methode ```public static int groesste(int[] refArray)``` berechnen und als ```return``` -Wert zurückgeben.
- Die kleinste Zahl in der Methode ```public static int keinste(int[] refArray)``` berechnen und als ```return``` -Wert zurückgeben.
- Die zweitkleinste Zahl in der Methode ```public static int zweitkleinste(int[] refArray)``` berechnen und als ```return``` -Wert zurückgeben.
- Die Zahlen in umgekehrter Reihenfolge und nur mit ihren Absolutbeträgen in ein anderes Array schreiben. Kopieren Sie hierzu die Absolutbeträge der Zahlen in umgekehrter Reihenfolge in ein anderes Array und geben Sie dieses als **return** -Wert zurück. Verwenden Sie die Java Standard Methode **int abs(int i)**.

[Source Code](48-array-utils/main.java)

## Aufgabe 49

Schreiben Sie eine **rekursive** Methode** für die Ackermann-Funktion.

Die Ackermann-Funktion ist eine Abbildung von zwei ganzen Zahlen ```n``` und ```m``` auf eine ganze Zahl ```a(n,m)```:

    a(0,m) = m + 1

    a(n,0) = a(n - 1, 1)

    a(n,m) = a(n - 1, a(n, m - 1))

Schreiben Sie auch eine ```main``` -Methode, die zwei Zahlen von der Tastatur einliest und dann das Ackermann-Ergebnis 
für diese Zahlen ausgibt. Bitte testen Sie, ab wann der Berechnungsaufwand zu groß wird, so dass wir nicht auf das Ergebnis warten können.

[Source Code](49-ackermann-recursive/main.java)

## Aufgabe 50

Beantworten Sie die folgenden Fragen schriftlich:

### In Aufgabe 45 haben Sie die Java-Methode **pow** verwendet. Wieso funktioniert **Math.pow(x,y)**, ohne dass wir den vollständigen Class Path angeben? Was passiert wohl automatisch mit **java.lang**?
Java.lang beinhaltet die CORE Funktionen der Java Sprache und werden vom Compiler automatisch inkludiert. 
Daher müssen Java.lang Pakete auch nicht importiert werden.

### Paramater Handling in Java-Methoden im Vergleich zu C:
Bitte schauen Sie sich diese beiden Programme an.


```c
#include <stdio.h>

void add(int a, int b, int *c) {
        *c = a + b;
}

int  main() {

        int x, y, z;

        printf("\nBitte ersten Wert eingeben: ");

        scanf("%i", &x);

        printf("\nBitte zweiten Wert eingeben: ");

        scanf("%i", &y);

        add(x, y, &z);

        printf("\n%i + %i = %i\n", x, y, z);

        return 0;

}
```

```java
import java.util.Scanner;

public class AddierePointer 
{
  private static Scanner in;

  public static void add(int a, int b, int c){ c = a + b;}

  public static void main(String[] args) {
        in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();

        int z = 0;

        add(x, y, z);

        System.out.println("Ergebnis von " + x + "+" + y + "ist: " + z);

  }
} 
```

#### Wird tatsächlich der Wert der Addition im Java Beispiel zurückgegeben?
Nein

#### Wie funktioniert die Parameterübergabe in Java (call by value / call by reference – **Vorsicht: Fangfrage!** )?
In Java werden Parameter immer per call-by-value übergeben. Auch Referenzen. 
Bei veränderbaren Objekten sind Veränderungen in der Methode auch beim Aufrufer sichtbar. 
Bei unveränderbaren Typen, wie beispielsweise primitiven Datentypen und Strings, merkt der Aufrufer nichts. 

#### Haben Sie eine Idee, wie man das Java-Programm verbessern könnte?
```java
  public static void add(int a, int b){
        return a + b;
  } 

  public static void main(String[] args) {
            ....
          int z = add(x, y);
            ....
    }
```

### Beschreiben Sie auf einer Seite, was Sie unter Objektorientierung verstehen.

### Was versteht man unter einem abstrakten Datentyp (ADT). Geben Sie zunächst eine allgemeine Definition an. Zeigen Sie dann am Beispiel einer Warteschlange, was ein ADT ist.


> In computer science, an abstract data type (ADT) is a mathematical model for a certain class of data structures that 
> have similar behavior; or for certain data types of one or more programming languages that have similar semantics. 
> An abstract data type is defined indirectly, only by the operations that may be performed on it and by mathematical 
> constraints on the effects (and possibly cost) of those operations.

Source: [Wikipedia](https://en.wikipedia.org/wiki/Abstract_data_type)

Ein Beispiel für ADT´s in Java ist das "List" Interface. Das Interface selber definiert keine Funktionalität weil es keine
grundlegende List Klasse gibt. Das Interface definiert nur ein Set von Methoden welche andere Klassen dann implementieren. (Zb: ArrayList und LinkedList)