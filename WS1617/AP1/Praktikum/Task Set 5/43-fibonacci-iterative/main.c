#include <stdio.h>

int main(void)
{
    int n, first = 0, second = 1, next, i;

    printf("Fibonacci Folge (Iterative)\n");

    printf("\nLänge der Fibonacci Folge: ");
    scanf("%d",&n);

    printf("Die ersten %d Ziffern der Fibonacci Folge :\n",n);


    for (i=0;i<n;i++) {
      if ( i <= 1 ){
        next = i;
      }else{
         next = first + second;
         first = second;
         second = next;
      }

      printf("%d\n",next);
   }

    return 0;
}