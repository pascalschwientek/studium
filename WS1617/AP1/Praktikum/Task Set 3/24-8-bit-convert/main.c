#include <stdio.h>
#include <stdlib.h>

char *int_to_bin(int n)
{
   int c, d, count;
   char *pointer;

   count = 0;
   pointer = (char*)malloc(8+1);

   if ( pointer == NULL )
      exit(EXIT_FAILURE);

   for ( c = 7 ; c >= 0 ; c-- )
   {
      d = n >> c;

      if ( d & 1 )
         *(pointer+count) = 1 + '0';
      else
         *(pointer+count) = 0 + '0';

      count++;
   }
   *(pointer+count) = '\0';

   return  pointer;
}

int main(void)
{
    int n, c, k;
    char *pointer;

    printf("Programm Dualzahl\n");

    printf("Geben Sie bitte eine ganze Zahl ein: ");
    scanf("%d",&n);

    if(n > 255 || n < 0) {
        printf("%d kann nicht als 8-Bit Dualzahl dargestellt werden\n", n);
        return 1;
    }

    pointer = int_to_bin(n);
    printf("8-Bit Dualzahl darstellung: %s\n", (void*)pointer);

    free(pointer);

    return 0;
}
