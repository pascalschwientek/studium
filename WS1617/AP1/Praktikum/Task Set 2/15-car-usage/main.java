import java.util.Scanner;

public class main
{
    private static Scanner in;

    public static void main(String[] args)
    {
        int benzinmenge;
        int wegstrecke;
        double benzinverbrauch;

        System.out.println("Programm zur Berechnung des Benzinverbrauchs eines Autos \n");

        System.out.println("Bitte geben sie die verbrauchte Benzinmenge in Liter ein: ");
        Scanner a = new Scanner(System.in);
        benzinmenge = a.nextInt();
        a.close();

        System.out.println("Bitte geben sie die gefahrenen Kilometer ein: ");
        Scanner b = new Scanner(System.in);
        wegstrecke = b.nextInt();
        b.close();

        benzinverbrauch = benzinmenge * 100 / wegstrecke;
        System.out.println("Ihr Wagen verbraucht "+ benzinverbrauch +" Liter Benzin auf 100 Kilometer");
    }
}