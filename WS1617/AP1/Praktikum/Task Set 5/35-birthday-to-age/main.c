#include <stdio.h>

#define NOW 2016

int birthyearInput()
{
    int birthyear;

    printf("Bitte geben Sie Ihr Geburtsjahr ein: ");
    scanf("%d", &birthyear);

    return birthyear;
}

int ageCalc(int birthyear)
{
    return NOW - birthyear;
}

int ageOutput(int age)
{
    printf("Sie sind heute %d Jahre alt.", age);

    return 0;
}

int main(void)
{
    printf("Programm zur Berechnung eines Alters einer Person\n");

    return ageOutput(ageCalc(birthyearInput()));
}
