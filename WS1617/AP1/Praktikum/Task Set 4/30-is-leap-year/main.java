import java.util.Scanner;

public class main
{
    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        int year;

        System.out.println("Programm Schaltjahr");

        System.out.print("Bitte geben Sie eine Jahreszahl ein: ");
        year = STDIN_SCANNER.nextInt();

        if(year % 400 == 0) {
            System.out.println(year + " ist ein Schaltjahr!");
        } else if(year % 100 == 0) {
            System.out.println(year + " ist kein Schaltjahr!");
        } else if(year % 4 == 0) {
            System.out.println(year + " ist ein Schaltjahr!");
        } else {
            System.out.println(year + " ist kein Schaltjahr!");
        }
    }
}