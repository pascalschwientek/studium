package Task17;

import java.util.Scanner;

public class main
{
    private static Scanner in;

    public static void main(String[] args)
    {
        int zahl1;
        int zahl2;
        int ergebnis;
        char op, wd = 'n';
        do {
            ergebnis = 0;
            System.out.println("Operator eingeben: ");
            Scanner a = new Scanner(System.in);
            op = a.next().charAt(0);
            System.out.println("CHARACTER: "+op);
            System.out.println("Erste Zahl: ");
            zahl1 = a.nextInt();
            System.out.println("Zweite Zahl: ");
            zahl2 = a.nextInt();
            switch(op) {
                case '+':
                    ergebnis = zahl1 + zahl2;
                    break;
                case '-':
                    ergebnis = zahl1 - zahl2;
                    break;
                case '*':
                    ergebnis = zahl1 * zahl2;
                    break;
                case '/':
                    if(zahl2 != 0) {
                        ergebnis = zahl1 / zahl2;
                    } else {
                        System.out.println("Man darf nicht durch NULL teilen!.\n");
                    }
                    break;
                default:
                    System.out.println("Nicht moeglich, Fehler.");
            }
            System.out.println("Ergebnis = "+ ergebnis +" \n");
            if(ergebnis != 0) {
                do {
                    System.out.println("Vorgang wiederholen? (y/n)");
                    wd = a.next().charAt(0);
                }while (wd=='y' && wd=='n');
            }
        }
        while((wd=='y'));
    }
}