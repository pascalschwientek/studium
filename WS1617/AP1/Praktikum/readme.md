# Programmer's Handbook

| **Nr.** | **Nachname** | **Vorname** | **Studien-gang** | **Mat Nr**  | **ADV Kennung** |
| ------- | ------------ | ----------- | ---------------- | ----------- | --------------- |
|  **1**  | Schwientek   | Pascal      | AI               | 01111461016 | inf451          |
|  **2**  | Karsch       | Konstantin  | AI               | 01111787811 | inf557          |



Das Programmer's Handbook enthält alle Aufgaben und Ihre Lösungen zum Praktikum "Algorithmen und Programmierung I" im WS 2016/2017.

Die Aufgaben werden wöchentlich in ILIAS bereitgestellt. Kopieren Sie diese in Ihr Programmer's Handbook und ergänzen Sie, nachdem Sie die Aufgaben gelöst haben, Ihre Lösungen in Form des Programmcodes. Es gibt auch Aufgaben, wo Fragen schriftlich zu beantworten sind. Auch diese Lösungen schreiben Sie bitte in Ihr Programmer's Handbook.

Sie arbeiten in **Teams** mit einer **max. Größe von 3 Personen.**

Es gibt 2 Abnahmetermine (Anfang Dez. 2016 und Jan. 2017), an denen wir uns Ihr Programmer's Handbook ansehen und Ihren Wissensstand überprüfen.

Zu diesen Terminen muss jeder sein Programmer's Book ausgedruckt mitbringen. Wir stellen Ihnen dann Fragen, die Sie selbstständig beantworten müssen.

Selbstverständlich gibt es neben den beiden Abnahmeterminen weitere Praktikumstermine, die zur Beratung gedacht sind. Sie können an diesen Terminen mit unseren Mitarbeitern Probleme lösen und Fragen hinsichtlich der Aufgaben stellen. Sie können in dieser Zeit natürlich auch mit ihrer Gruppe die Aufgaben in den Praktikaräumen bearbeiten.



## Übungsblatt 1

[Link](Task%20Set%201)

**Kommentare zur Bearbeitung von Übungsblatt 1:**

- Benutzte Hilfsmittel (gelesene Seiten im Script, Bücher, Links)
- Schwierigkeiten / Probleme
- Empfundener Schwierigkeitsgrad: leicht / mittel / hoch


## Übungsblatt 2

[Link](Task%20Set%202)

**Kommentare zur Bearbeitung von Übungsblatt 2:**

- Benutzte Hilfsmittel (gelesene Seiten im Script, Bücher, Links)
- Schwierigkeiten / Probleme
- Empfundener Schwierigkeitsgrad: leicht / mittel / hoch



## Übungsblatt 3

[Link](Task%20Set%203)

**Kommentare zur Bearbeitung von Übungsblatt 3:**

- Benutzte Hilfsmittel (gelesene Seiten im Script, Bücher, Links)
- Schwierigkeiten / Probleme
- Empfundener Schwierigkeitsgrad: leicht / mittel / hoch

## Übungsblatt 4

[Link](Task%20Set%204)

**Kommentare zur Bearbeitung von Übungsblatt 4:**

- Benutzte Hilfsmittel (gelesene Seiten im Script, Bücher, Links)
- Schwierigkeiten / Probleme
- Empfundener Schwierigkeitsgrad: leicht / mittel / hoch


## Übungsblatt 5

[Link](Task%20Set%205)

**Kommentare zur Bearbeitung von Übungsblatt 5:**

- Benutzte Hilfsmittel (gelesene Seiten im Script, Bücher, Links)
- Schwierigkeiten / Probleme
- Empfundener Schwierigkeitsgrad: leicht / mittel / hoch

## Übungsblatt 6

[Link](Task%20Set%206)

**Kommentare zur Bearbeitung von Übungsblatt 6:**

- Benutzte Hilfsmittel (gelesene Seiten im Script, Bücher, Links)
- Schwierigkeiten / Probleme
- Empfundener Schwierigkeitsgrad: leicht / mittel / hoch