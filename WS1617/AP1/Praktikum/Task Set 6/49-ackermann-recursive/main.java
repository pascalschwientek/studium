import java.util.Scanner;

public class main
{
    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static int Ack(int m, int n) {
        if(m < 0 || n < 0) return 0;
        if (m == 0)        return n + 1;
        if (n == 0)        return Ack(m - 1, 1);
        return Ack(m - 1, Ack(m, n - 1));

    }

    public static void main(String[] args) {
        int a,b, ack;
        System.out.println("Programm Ackermann Funktion (R)");

        System.out.print("Bitte geben Sie zwei Zahlen ein: ");
        a = STDIN_SCANNER.nextInt();
        b = STDIN_SCANNER.nextInt();

        ack = Ack(a,b);

        System.out.println(Integer.toString(ack));
    }
}