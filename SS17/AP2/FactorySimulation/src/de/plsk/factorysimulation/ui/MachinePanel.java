package de.plsk.factorysimulation.ui;

import de.plsk.factorysimulation.Machine;
import de.plsk.factorysimulation.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MachinePanel extends JPanel {
    private PanelManager panelManager;

    public MachinePanel(PanelManager panelManager) {
        super();
        this.panelManager = panelManager;
    }

    public void updateData() {
        this.removeAll();
        this.repaint();

        JPanel outerPanel = new JPanel();
        outerPanel.setLayout(new BoxLayout(outerPanel, BoxLayout.Y_AXIS));

        JPanel header = new JPanel();
        header.add(new JLabel("Maschine"));
        header.add(new JLabel("Erzeugnis"));
        header.add(new JLabel("Abhängigkeit"));
        header.setBackground(new Color(0,0,0,30));

        outerPanel.add(header);


        Iterator<Machine> iterator = panelManager.getMachinesDraft().iterator();
        int index = 0;

        while(iterator.hasNext()) {
            Machine machine = iterator.next();

            JPanel itemPanel = this.makeItemPanel(
                    machine,
                    index
            );

            outerPanel.add(itemPanel);

            index++;
        }
        this.add(outerPanel, BorderLayout.NORTH);
        this.revalidate();
    }

    private JPanel makeItemPanel(Machine machine, int index) {
        JPanel innerPanel1 = new JPanel();
        JPanel innerPanel2 = new JPanel();
        JPanel innerPanel3 = new JPanel();

        innerPanel1.setLayout(new BoxLayout(innerPanel1, BoxLayout.Y_AXIS));
        innerPanel1.setBackground(Color.lightGray);
        innerPanel1.setBorder(BorderFactory.createLineBorder(Color.black));
        innerPanel2.setLayout(new BoxLayout(innerPanel2, BoxLayout.Y_AXIS));
        innerPanel2.setBackground(Color.lightGray);
        innerPanel2.setBorder(BorderFactory.createLineBorder(Color.black));
        innerPanel3.setLayout(new BoxLayout(innerPanel3, BoxLayout.Y_AXIS));
        innerPanel3.setBackground(Color.lightGray);
        innerPanel3.setBorder(BorderFactory.createLineBorder(Color.black));

        JLabel machineDataType = new JLabel("Type:   " + machine.getType());
        JLabel machineDataName = new JLabel("Name:   " + machine.getName());
        JLabel machineDataCost = new JLabel("Kosten: " + machine.getCost());
        innerPanel1.add(machineDataType);
        innerPanel1.add(machineDataName);
        innerPanel1.add(machineDataCost);

        Product producing = machine.getProducing();
        JLabel productDataName  = new JLabel("Name:               " + producing.getName());
        JLabel productDataCost  = new JLabel("Herstellungskosten: " + producing.getCost());
        JLabel productDataValue = new JLabel("Verkaufswert:       " + producing.getValue());
        innerPanel2.add(productDataName);
        innerPanel2.add(productDataCost);
        innerPanel2.add(productDataValue);

        if(machine.getType() == "Processor")
        {
            Product dependency = machine.getDependency();

            JLabel dependencyDataName  = new JLabel("Name:               " + dependency.getName());
            JLabel dependencyDataCost  = new JLabel("Herstellungskosten: " + dependency.getCost());
            JLabel dependencyDataValue = new JLabel("Verkaufswert:       " + dependency.getValue());
            JLabel dependencyDataCount = new JLabel("Anzahl:             " + machine.getDependencyCount());

            innerPanel3.add(dependencyDataName);
            innerPanel3.add(dependencyDataCost);
            innerPanel3.add(dependencyDataValue);
            innerPanel3.add(dependencyDataCount);
        }

        JPanel itemPanel = new JPanel();
        itemPanel.setLayout(new GridLayout(0,3));

        itemPanel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                panelManager.addMachineToFactory(index);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                itemPanel.setBorder(BorderFactory.createLineBorder(Color.red));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                itemPanel.setBorder(null);
            }
        });





        itemPanel.add(innerPanel1);
        itemPanel.add(innerPanel2);
        itemPanel.add(innerPanel3);

        return itemPanel;
    }
}
