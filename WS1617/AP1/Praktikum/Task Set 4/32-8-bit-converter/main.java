import java.util.Scanner;

public class main
{
    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        int integer;
        String integer_binary;

        System.out.println("Programm Dualzahl");

        System.out.print("Geben Sie bitte eine ganze Zahl ein: ");
        integer = STDIN_SCANNER.nextInt();

        if(integer > 255 || integer < 0) {
            System.out.println(Integer.toString(integer) + " kann nicht als 8-Bit Dualzahl dargestellt werden");
            System.exit(1);
            return;
        }

        integer_binary = Integer.toBinaryString(integer);

        System.out.println("8-Bit Dualzahl darstellung: " + integer_binary);
    }
}