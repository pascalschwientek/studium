#include <stdio.h>

int main(void)
{
    float netto;

    printf("Programm zur Berechnung eines Bruttobetrages\n");

    printf("\nBitte geben Sie den Nettobetrag in EUR ein: ");
    scanf("%f", &netto);

    printf("Nettobetrag = %.2f EUR \n", netto);
    printf("Mehrwertsteuersatz 19.00%% = %.2f EUR \n", netto * 0.19);
    printf("Bruttobetrag = %.2f EUR \n", (netto * 0.19) + netto);

    return 0;
}
