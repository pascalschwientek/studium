package de.plsk.factorysimulation;

//TODO: why is this inheriting from Producer and not from Machine?
public class Processor extends Producer {
    private Product dependency;
    private int dependencyCount;

    /**
     * TODO javadoc
     *
     * @param name
     * @param cost
     */
    public Processor(String name, Double cost, Product producing, Product dependency, int dependencyCount) {
        super(name, cost, producing);

        this.dependency = dependency;
        this.dependencyCount = dependencyCount;

        this.type = "Processor";
    }

    /**
     * @return Dose the factory have sufficient credit funds?
     */
    private boolean sufficientDependencies() {
        return getFactory().getStock().count(dependency.getName()) >= dependencyCount;
    }

    /**
     * TODO javadoc
     */
    public Product getDependency() {
        return dependency;
    }

    /**
     * TODO javadoc
     */
    public int getDependencyCount() {
        return dependencyCount;
    }

    /**
     * TODO javadoc
     */
    @Override
    protected void produce() {
        if(sufficientDependencies()) {
            super.produce();

            getFactory().getStock().remove(dependency.getName(), dependencyCount);
        } else {
            double cost = dependency.getCost() * 2;
            double required = dependencyCount * cost;

            if(getFactory().getTestcredit() >= required) {

                for (int i = 0; i < dependencyCount; i++) {
                    getFactory().getStock().add(dependency);
                    System.out.println("Added");
                }

                getFactory().reduceTestcredit(required);
            }

            produce();
        }
    }
}
