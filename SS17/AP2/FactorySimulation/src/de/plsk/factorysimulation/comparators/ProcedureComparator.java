package de.plsk.factorysimulation.comparators;

import de.plsk.factorysimulation.Machine;

import java.util.Comparator;

public class ProcedureComparator implements Comparator<Machine> {

    @Override
    public int compare(Machine machine1, Machine machine2) {
        if(machine1.getDependencyCount() > 0 && machine2.getDependencyCount() > 0) return 0;
        if(machine1.getDependencyCount() == 0 && machine2.getDependencyCount() > 0) return 1;

        return -1;
    }
}
