#include <stdio.h>

int main(void)
{
    int array[5], i;

    printf("Programm Array Ein- und Ausgabe\n");

    for(i=0; i<5; i++) {
    	printf("Bitte geben Sie den %d. Wert ein: ", i+1);
        scanf("%d", &array[i]);
    }

    printf("Sie haben folgende Werte eingegeben: ");

    for(i=0; i<5; i++) {
    	printf("%d", array[i]);
        if(i<=3){
            printf(",");
        }else{
            printf("\n");
        }
    }

    return 0;
}
