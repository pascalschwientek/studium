package de.plsk.factorysimulation.comparators;

import de.plsk.factorysimulation.Machine;

import java.util.Comparator;

public class NameComparator implements Comparator<Machine> {

    @Override
    public int compare(Machine machine1, Machine machine2) {
        return machine1.getName().compareTo(machine2.getName());
    }
}
