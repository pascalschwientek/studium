import java.util.Scanner;

public class main
{
    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static int ggt_U(int a_U, int b_U) {
        if(a_U == 0) {
            return b_U;
        }

        while(b_U != 0) {
            if(Integer.compareUnsigned(a_U, b_U) > 0) {
                a_U = a_U - b_U;
            } else {
                b_U = b_U - a_U;
            }
        }

        return a_U;
    }

    public static void main(String[] args) {
        int a_U, b_U, factor_U;

        System.out.println("Programm zum Kürzen von Brüchen");

        System.out.print("\nBitte geben Sie den Zähler ein: ");
        a_U = STDIN_SCANNER.nextInt();

        System.out.print("\nBitte geben Sie den Nenner ein: ");
        b_U = STDIN_SCANNER.nextInt();

        if(Integer.compareUnsigned(a_U, b_U) < 0) {
            System.out.print("Zwei positive Zahlen a und b mit a >= b nicht gegeben");
            System.exit(1);
            return;
        }

        factor_U = ggt_U(a_U, b_U);

        System.out.println("\n---\n");
        System.out.println("Der größte gemeinsame Teiler ist: " + Integer.divideUnsigned(b_U, factor_U));
        System.out.println("Der gekürzte Bruch heißt: " + Integer.divideUnsigned(a_U, factor_U) + "/" + Integer.divideUnsigned(b_U, factor_U));
    }
}