package de.plsk.factorysimulation;

public class Product {
    private double cost;
    private double value;
    private String name;

    /**
     * Initialize a new Product
     *
     * @param name the name of the product
     * @param cost the production cost of the product
     * @param value the re-sell value of the product
     */
    public Product(String name, double cost, double value) {
        setCost(cost);
        setValue(value);
        setName(name);
    }

    /**
     * @return the production cost of the product
     */
    public double getCost() {
        return cost;
    }

    /**
     * @return the re-sell value of the product
     */
    public double getValue() {
        return value;
    }

    /**
     * @return the name of the product
     */
    public String getName() {
        return name;
    }

    /**
     * @param c the production cost of the product
     */
    private void setCost(Double c) {
        cost = c;
    }

    /**
     * @param v the re-sell value of the product
     */
    private void setValue(Double v) {
        value = v;
    }

    /**
     * @param n the name of the product
     */
    private void setName(String n) {
        name = n;
    }
}
