package de.plsk.factorysimulation.ui;

import de.plsk.factorysimulation.Factory;
import de.plsk.factorysimulation.Machine;
import de.plsk.factorysimulation.MachineList;
import de.plsk.factorysimulation.Product;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class PanelManager {

    private InputPanel inputPanel;
    private MachinePanel machinePanel;
    private MachineSelectionPanel machineSelectionPanel;

    public void start() {
        JFrame frame = new JFrame("Factory Simulation");
        frame.add(inputPanel, BorderLayout.WEST);
        frame.add(machinePanel, BorderLayout.CENTER);
        frame.setSize(1000, 500);
        frame.setVisible(true);

        JFrame selection = new JFrame("Deine Auswahl");
        selection.add(machineSelectionPanel);
        selection.setSize(200, 700);
        selection.setVisible(true);
    }

    private ArrayList<Product> productsDraft = new ArrayList<Product>();
    private MachineList machinesDraft = new MachineList();
    private Factory factory;

    public PanelManager(Factory factory)
    {
        this.factory = factory;
        this.inputPanel = new InputPanel(this);
        this.machinePanel = new MachinePanel(this);
        this.machineSelectionPanel = new MachineSelectionPanel(this);
    }

    public void addOrReplaceProductdraft(Product product) {
        for (int i = 0; i < productsDraft.size(); i++) {
            Product tmp = productsDraft.get(i);

            if(tmp.getName() == product.getName()) {
                productsDraft.remove(i);
                break;
            }
        }

        productsDraft.add(product);
        inputPanel.updateData();
    }

    public void addOrReplaceMachinedraft(Machine machine) {
        Iterator<Machine> iterator = machinesDraft.iterator();

        int index = 0;

        while(iterator.hasNext()) {
            Machine tmp = iterator.next();

            if(tmp.getName().compareTo(machine.getName()) == 0) {
                machinesDraft.remove(index);
                break;
            }

            index++;
        }

        machinesDraft.addLast(machine);
        machinePanel.updateData();
        machinePanel.repaint();
    }

    public void sort(Comparator comp) {
        factory.sort(comp);
        machineSelectionPanel.updateData();

    }

    public MachineList getMachinesDraft() {
        return machinesDraft;
    }

    public ArrayList<Product> getProductsDraft() {
        return productsDraft;
    }

    public void addMachineToFactory(int index) {
        Machine machine;

        try {
            machine = machinesDraft.get(index);
        } catch (NoSuchElementException e) {
            System.out.printf("Can't add machine to factory. No such element with index %d", index);
            return;
        } catch (IndexOutOfBoundsException e) {
            System.out.printf("Index of %d too big ot too small", index);
            return;
        }

        factory.addMachine(machine);

        inputPanel.updateData();
        machineSelectionPanel.updateData();
    }

    public void removeMachineFromFactory(int index) {
        factory.removeMachine(index);

        inputPanel.updateData();
        machineSelectionPanel.updateData();
    }

    public void resetFactory() {
        factory.resetFactory();

        machinesDraft.clear();
        productsDraft.clear();

        machineSelectionPanel.updateData();
        inputPanel.updateData();
        machinePanel.updateData();
    }

    public MachineList getActiveMachines() {
        return factory.getMachines();
    }

    public double getTestcredit() {
        return factory.getTestcredit();
    }

    public void simulate(Integer rounds) {
        //TODO
        factory.simulate(rounds);
    }
}
