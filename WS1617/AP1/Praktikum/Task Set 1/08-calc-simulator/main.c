#include <stdio.h>

int main(void)
{
    char operation;
    int repeat;
    float a, b;

    printf("Programm zur einfachen Simulation eines Teschenrechners\n");

    do {
        repeat = 0;
        printf("\nBitte geben Sie die gewünschte Rechenoperation ein: ");
        scanf("%c", &operation);

        printf("Zwei Zahlen eingeben: ");
        scanf("%f %f", &a, &b);

        switch(operation) {
        	case '+':
                printf("Die Summe der beiden Zahlen lautet: %f\n", a+b);
                break;
        	case '-':
                printf("Die Differenz der beiden Zahlen lautet: %f\n", a-b);
                break;
        	case '*':
                printf("Das Produkt der beiden Zahlen lautet: %f\n", a*b);
                break;
            case '/':
                if(b==0) printf("Die Division durch 0 ist nicht erlaubt\n"); return 1;
                printf("Die Quotient der beiden Zahlen lautet: %f\n", a/b);
                break;
            default:
                repeat = 1;
                break;
        }
    } while (repeat);

    return 0;

}
