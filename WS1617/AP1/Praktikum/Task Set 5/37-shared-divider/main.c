#include <stdio.h>

unsigned int ggt(unsigned int a, unsigned int b)
{
    if (a == 0) {return b;}

    while(b != 0){
    	if (a > b){
    		a = a - b;
    	}else{
    		b = b - a;
    	}
    }

    return a;
}

int main(void)
{
    unsigned int a, b, factor;

    printf("Programm zum Kürzen von Brüchen\n");

    printf("\nBitte geben Sie den Zähler ein: ");
    scanf("%d", &a);

    printf("Bitte geben Sie den Nenner ein: ");
    scanf("%d", &b);

    if (a < b) {
        printf("Zwei positive Zahlen a und b mit a >= b nicht gegeben");
        return 1;
    }

    factor = ggt(a, b);

    printf("\n---\n");
    printf("Der größte gemeinsame Teiler ist: %d\n", b/factor);
    printf("Der gekürzte Bruch heißt: %d/%d\n", a/factor, b/factor);

    return 0;
}
