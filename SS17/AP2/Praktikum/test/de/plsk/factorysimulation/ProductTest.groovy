package de.plsk.factorysimulation

class ProductTest extends GroovyTestCase {
    void testGetters() {
        String name = "Fidget Spinner"
        Double cost = 5.28
        Double value = 9.99

        Product product = new Product(name, cost, value)

        assertTrue(product.getName().matches(name))
        assertEquals(cost, product.getCost())
        assertEquals(value, product.getValue())
    }
}
