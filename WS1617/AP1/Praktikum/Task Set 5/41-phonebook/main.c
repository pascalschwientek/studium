#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LIST_LENGTH 10

#define LIST_ERROR  5
#define OK          0

static int position;

struct Contact {
    char last_name[30];
    char first_name[30];
    char prefix[30];
    char number[30];
};

struct Contact list[LIST_LENGTH];










int search_entry(struct Contact *list, char name[])
{
    for (int i = 0; i < LIST_LENGTH; i++) {
        if(*list[i].last_name == name){
            return i;
        }
    }

    return -1;
}

int add_entry(int *position, struct Contact *list, struct Contact contact)
{
    if(&position >= LIST_LENGTH)
    {
        printf("Die Kontaktliste ist bereits voll\n");
        return LIST_ERROR;
    }

    if(search_entry(list, *contact.last_name) < 0)
    {
        printf("Dieser Name ist bereits in der Kontaktliste.\n");
        return LIST_ERROR;
    }

    list[*position+1] = contact;
    position++;

    return OK;
}

int rm_entry(struct Contact *list, int *global_position, int position)
{
    for(int i = &position; i < LIST_LENGTH - 1; i++){
        list[i] = list[i + 1];
    }

    global_position = &global_position - 1;

    return OK;
}













int menu_add_entry(int *position, struct Contact *list)
{
    struct Contact contact;

    printf("Vorname: ");
    do
    {
        scanf("%c",&contact.first_name);
    } while(getchar()!= '\n');


    printf("Nachname: ");
    fgets(contact.last_name, 30, stdin);

    printf("Vorwahl: ");
    fgets(contact.prefix, 30, stdin);

    printf("Telefonnummer: ");
    fgets(contact.number, 30, stdin);

    return add_entry(position, list, contact);

}

int menu_rm_entry(int *global_position, struct Contact *list)
{
    int position;
    char name[30];

    printf("Nachname: ");
    fgets(name, 30, stdin);

    position = search_entry(list, name);

    return rm_entry(list, global_position, position);
}

int menu_search_entry(struct Contact *list)
{
    char name[30];
    int position;
    struct Contact contact;

    printf("Name nachdem gesucht werden soll: ");
    fgets(name, 30, stdin);

    position = search_entry(list, name);
    contact = list[position];

    printf("Name............: %s\n", contact.last_name);
    printf("Vorname.........: %s\n", contact.first_name);
    printf("Vorwahl.........: %s\n", contact.prefix);
    printf("Telefonnummer...: %s\n", contact.number);

    return OK;
}

void menu()
{
    int selection;

    do
    {
        printf("-1- Neuer Eintrag\n");
        printf("-2- Eintrag Löschen\n");
        printf("-3- Eintrag Suchen\n");
        printf("\nIhre Auswahl : ");
        scanf("%d",&selection);
        switch(selection)
        {
            case 1 : menu_add_entry(&position, &list);   break;
            case 2 : menu_rm_entry(&position, &list);    break;
            case 3 : menu_search_entry(&list);           break;
            default: break;
        }
    } while(selection < 4);
}

int main(void)
{
    menu();

    return 0;
}