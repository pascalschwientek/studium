package de.plsk.factorysimulation

class ProducerTest extends GroovyTestCase {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream()
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream()

    void setUp() {
        super.setUp()

        System.setOut(new PrintStream(outContent))
        System.setErr(new PrintStream(errContent))
    }

    void tearDown() {
        System.setOut(null)
        System.setErr(null)
    }

    void testStartMachine() {
        Producer p = new Producer("Fidget Spinner Producer", 0.25, new Product("Fidget Spinner", 0.25, 0.50))
        p.setFactory(new Factory("ChinaCopyCat"))
        p.factory.testcredit = 100

        p.startMachine()

        assertEquals(String.format("Fidget Spinner Producer has started.%nFidget Spinner produced.%n"), outContent.toString())
    }
}
