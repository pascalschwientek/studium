#include <stdio.h>

int main(void)
{
    int matrix[5][5], row, column, option;

    for (row=0; row<5; row++)
    {
        for(column=0; column<5; column++)
        {
            matrix[row][column] = 0;
            printf("%d", 0);
        }
        printf("\n");
    }

    printf("Wie soll die Matrix gefüllt werden?\n");
    printf("Option 1: Fülle die erste Zeile und letzte Spalte mit Einsen\n");
    printf("Option 2: Fülle die beiden Diagonalen der Matrix mit Einsen\n");
    scanf("%d", &option);

    switch (option) {
        case 1:
        for(column=0; column<5; column++)
        {
            matrix[1][column] = 1;
            matrix[5][column] = 1;
        }
        break;
        case 2:
        for (row=0; row<5; row++)
        {
            matrix[row][row] = 1;
            matrix[row][4-row] = 1;
        }
        break;
    }

    for (row=0; row<5; row++)
    {
        for(column=0; column<5; column++)
        {
            printf("%d", matrix[row][column]);
        }
        printf("\n");
    }

    return 0;
}
