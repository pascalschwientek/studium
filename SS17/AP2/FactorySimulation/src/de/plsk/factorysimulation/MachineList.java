package de.plsk.factorysimulation;

import de.plsk.factorysimulation.exceptions.UnsortedListException;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MachineList implements Iterable<Machine> {
    private Node first = null;

    private boolean sorted = false;

    @Override
    public Iterator<Machine> iterator() {
        return new Iterator<Machine>() {

            Node runPointer = first;

            @Override
            public boolean hasNext() {
                return runPointer != null ;
            }

            @Override
            public Machine next() {
                if (runPointer == null) throw new NoSuchElementException();

                Machine machine = runPointer.machine;

                runPointer = runPointer.next;

                return machine;
            }
        };
    }

    class Node {
        private Machine machine;

        private Node next = null;

        public Node(Machine machine, Node next)
        {
            this.machine = machine;
            this.next = next;
        }

        public Node(Machine machine)
        {
            this.machine = machine;
        }
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void addFirst(Machine machine) {
        first = new Node(machine, first);

        sorted = false;
    }

    public void addLast(Machine machine) {
        if(isEmpty()) {
             addFirst(machine);
             return;
        }

        Node runPointer = first ;
        while (runPointer.next != null) {
            runPointer = runPointer.next;
        }

        runPointer.next = new Node(machine);

        sorted = false;
    }

    public int size() {
//        if(isEmpty()) return 0;
//
//        int size = 0;
//        Node runPointer = first;
//
//        while(runPointer != null){
//            size++;
//            runPointer = runPointer.next;
//        }
//
//        return size;

        return size(first);
    }

    public int size(Node node) {
        if(node == null) {
            return 0;
        }

        int result = size(node.next) + 1;

        return result;
    }

    public int indexOf(Machine machine) {
        Node node = first;
        int index = 0;

        do {
            if(machine.getName() == node.machine.getName())
            {
                return index;
            }

            index++;
            node = node.next;

        } while (node.next != null);

        return -1;
    }

    public Machine get(int i) {
        if (isEmpty()) throw new NoSuchElementException();

        int index = 0;
        Node runPointer = first;

        while (runPointer != null){
            if(index == i) return runPointer.machine;
            index++;
            runPointer = runPointer.next;
        }

        throw new NoSuchElementException();
    }

    public Machine getFirst() {
        if(isEmpty()) return null;

        return first.machine;
    }

    public void set(int i, Machine machine) {
        if(isEmpty()) throw new NoSuchElementException();
        if(i < 0 || i >= size()) throw new IndexOutOfBoundsException();

        Node runPointer = first;
        int index = 0;

        while (i != index){
            index++;
        }

        runPointer.machine = machine;
        sorted = false;
    }

    public void remove(int i) {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        if (i == 0) {
            first = first.next;
            return;
        }

        int counter = 1;
        Node runPointer = first;

        while (runPointer.next != null) {
            if (counter == i) {
                runPointer.next = runPointer.next.next;
                return;
            }
            runPointer = runPointer.next;
            counter++;
        }

        throw new NoSuchElementException();
    }

    public void clear() {
        first = null;
        sorted = false;
    }

    public void addSorted(Machine machine, Comparator comp) {
        //if ( !sorted ) throw new UnsortedListException();

        if (isEmpty () || comp.compare(machine, getFirst()) <= 0) {
            addFirst(machine);
            sorted = true;
        } else {
            Node runPointer = first;

            while (runPointer.next != null && comp.compare(machine, runPointer.next.machine) > 0 ) {
                runPointer = runPointer.next;
            }

            runPointer.next = new Node (machine , runPointer.next);
        }
    }

    public void sort(Comparator<Machine> comp) {
        Node temp = first;

        clear();

        while (temp != null) {
            addSorted(temp.machine, comp);
            temp = temp.next;
        }
    }
}
