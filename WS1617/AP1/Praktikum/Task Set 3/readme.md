# Übungsblatt 3

## Aufgabe 19

Schreiben Sie ein C-Programm das per Benutzereingabe fünf ganzzahlige Werte in ein Array speichert. Anschließend sollen die Werte aus dem Array gelesen und formatiert ausgegeben werden. Bitte benutzen Sie für die Ein- sowie die Ausgabe jeweils eine Schleife.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm Array Ein- und Ausgabe

Bitte geben Sie den 1. Wert ein: ___
Bitte geben Sie den 2. Wert ein: ___
Bitte geben Sie den 3. Wert ein: ___
Bitte geben Sie den 4. Wert ein: ___
Bitte geben Sie den 5. Wert ein: ___

Sie haben folgende Werte eingegeben: x, x, …, x
```

[Source Code](19-build-array/main.c)

## Aufgabe 20

Schreiben Sie ein C-Programm, das den Median von sechs Werten bestimmt. Die Werte sollen per Benutzereingabe in einem Array gespeichert werden. Anschließend soll der Median der Werte bestimmt und dieser auf der Konsole ausgegeben werden.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm Median

Bitte geben Sie den 1. Wert ein: ___
Bitte geben Sie den 2. Wert ein: ___
Bitte geben Sie den 3. Wert ein: ___
Bitte geben Sie den 4. Wert ein: ___
Bitte geben Sie den 5. Wert ein: ___
Bitte geben Sie den 6. Wert ein: ___

Der Median der eingegebenen Zahlenfolge ist: xx
```

**Hinweis:** Diese Aufgabe ist nicht so ganz einfach. Es wird hier der Median bestimmt und nicht das arithmetische Mittel. Sie müssen sich also zunächst überlegen, wie Sie die Zahlen der Größe nach im Array sortieren. Sie können dazu ein einfaches Verfahren Ihrer Wahl verwenden. Informieren Sie sich in der Literatur über einfache Sortieralgorithmen.

[Source Code](20-median/main.c)

## Aufgabe 21

Schreiben Sie ein C-Programm, das die Primzahlen zwischen 2 bis maximal 1000 berechnet und zwar nach der altehrwürdigen Methode „Sieb des Eratosthenes&quot; (3. Jahrhundert. v. Chr.): Das Verfahren berechnet für eine eingegebene natürliche Zahl (&gt;1) alle Primzahlen bis einschließlich dieser Zahl. Für falsche oder gar keine Eingaben geben Sie bitte eine entsprechende Fehlermeldung aus.

**Hinweis:** Verwenden Sie ein int-Array, dessen Komponenten 0oder 1 sein können. Das Ziel besteht darin, dem i-ten Element den Wert 1 zu zuweisen, falls i eine Primzahl ist, und andernfalls den Wert 0. Dies wird erreicht, indem für jedes ialle Elemente des Arrays, die einem beliebigen Vielfachen von i entsprechen, auf 0 gesetzt werden. Danach wird das Feld nochmals durchlaufen, um alle gefundenen Primzahlen auszugeben.

[Source Code](21-prime-calc/main.c)

## Aufgabe 22

Schreiben sie ein C-Programm, das ermittelt, ob es sich bei einer eingegebenen Jahreszahl um ein Schaltjahr handelt.

**Hilfestellung:** Ist die Jahreszahl durch 4 teilbar, aber nicht durch 100, so ist es ein Schaltjahr. Ist die Jahreszahl durch 100 teilbar, aber nicht durch 400, so ist es kein Schaltjahr. Ist die Jahreszahl durch 400 teilbar, dann ist es ein Schaltjahr.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm Schaltjahr

Bitte geben Sie eine Jahreszahl ein: 1993

1993 ist kein Schaltjahr!
```

**Hinweis:**  Geben Sie natürlich auch aus, wenn es sich um kein Schaltjahr handelt.

[Source Code](22-is-leap-year/main.c)

## Aufgabe 23

Schreiben Sie ein C-Programm, das eine 5\*5 Matrix voller Nullen erstellt und diese ausgibt. Anschließend sollen dem Benutzer zwei Auswahlmöglichkeiten angeboten werden, wie die Matrix gefüllt werden soll:

Option 1: Fülle die erste Zeile und letzte Spalte mit Einsen

Option 2: Fülle die beiden Diagonalen der Matrix mit Einsen

Nach der Auswahl soll die Matrix, wie vom Nutzer ausgewählt, gefüllt und ausgegeben werden.

**Hinweis:** Nutzen Sie zum Füllen der Matrix unbedingt Schleifen. Dies ist aus Übungszwecken eine wichtige Anforderung dieser Aufgabe.

[Source Code](23-matrix-generator/main.c)

## Aufgabe 24

Schreiben Sie ein C-Programm, das eine eingegebene positive ganze Zahl inklusive der 0 in eine 8-Bit Dualzahl umwandelt. Geben Sie eine Fehlermeldung aus, falls die eingegebene Zahl zu groß ist.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm Dualzahl

Geben Sie bitte eine ganze Zahl ein: 3

Ausgabe im Dualsystem: 00000011
```

[Source Code](24-8-bit-convert/main.c)

## Aufgabe 25

Schreiben Sie ein C-Programm, das zwei Integerzahlen (Typ int) einliest und addiert.

```
Programm Bereichsüberschreitungen

Geben Sie bitte zwei Integerzahlen ein: x, y

Die Summe von x und y ist: _______
```

Experimentieren Sie mit sehr großen Werten und erläutern Sie in 5 Sätzen, welche Effekte auftreten und woran das liegt.

[Source Code](25-int-sum/main.c)

**Antwort:** 
mit dem GCC Compiler können Integer 32 bit groß sein. Ein singed integer hält somit ganzzahlen von -(2^31) bis (2^31)-1 also −2,147,483,648 bis 2,147,483,647. Bei einer größeren eingabe komt es zu einem overflow.
Da wir im code aber nur einen integer (4byte) festgelegt haben wird nur ein teil gelesen.
1(1111111) + 1 = 1(00000000) aus 255 + 1 wird 0.

## Aufgabe 26

Beantworten Sie die folgenden Fragen schriftlich:

### Wozu dient eine Include-Datei in C und was enthält sie? Erläutern Sie auch den Unterschied zwischen #include <datei.h> und #inlcude "datei.h".
<datei.h> sucht auf System Ebene nach der includ datei und “datei.h“ sucht auf Projekt Ebene nach der includ datei.

### Was versteht man unter Evaluation eines Ausdrucks und wieso ist das in der Programmierung wichtig zu wissen? Zeigen Sie, wie der Ausdruck double x = 3 + 4.5 evaluiert wird und geben Sie an, wie der Wert des Ausdrucks ist.
Die Evaluation bezeichnet den Vorgang, der einem Ausdruck einen Wert zordnet.
Das Program versucht dem Ausdruck double x den Wert 3 + 4.5 zuzordnen, dieser ist allerdings selber eine expression die evaluiert werden muss. Sobald dies getan ist wird x der Wert 7.5 zugeordnet.

### Was bewirkt der Selektorausdruck in der switch-Anweisung? Welche Typen kann dieser Ausdruck haben?
der Selektorausdruck kann nur den selben typen wie die switch expression haben.

### Diskutieren Sie die Verwendung von goto in Programmen.
goto Anweisungen ermöglichen unbedintge Sprünge im Programm ablauf. Sie erschwerden das verfolgen des Programm ablaufs und machen es schwer diesen zu verstehen oder zu verändern. Generel sollte davon abgesehen werden goto anweisungen einzubauen. Ausnhamen können Programme sein wo das Ziel eine möglichst schweres debugging gewollt ist. (Zum beispiel DRM software) 

### Stellen Sie sich vor, Sie programmieren 3 ineinander geschaltete Schleifen. Die innere Schleife enthält eine Anweisung. Wie oft wird diese Anweisung ausgeführt, wenn jede Schleife 100 Schritte durchläuft? Begründen Sie Ihre Antwort.
Die innerste schleife wird 1.000.000 mal durchlaufen. 100^3. Die Äußerste schleife startet die mittlere schleife 100 mal. Diese startet nun also 100x die 100 schleifen durchgänge der inneren schleife.

### C führt keine Überprüfung der Arraygrenzen durch. Beschreiben Sie, was passiert, wenn Sie in Ihrem Programm die Arraygrenzen überschreiten. Verdeutlichen Sie Ihre Antwort durch ein konkretes Beispiel in C-Code.
Wenn man in seinem Programm die Arraygrenze überschreitet, sieht C das nicht als Fehler an aber im Programmablauf wird auf irgendeine Speicherzelle mit der absoluten Adresse “basisadresse  index*speicherbedarfProElement“ (Skript S.81) zugreifen wird. Das kann zum Programmabsturz führen. ``eingabe[5] = 4711;``

### Wieso macht es Sinn, grundsätzlich die Arraygröße als Konstante zu definieren? Begründen Sie dies und überprüfen Sie für Ihre Programme, ob Sie das gemacht haben!
Die Array-Größe wird unter umständen an meheren stellen im Programm genutzt. Möchte man diese ändern so muss der gesamter code überprüft werden um sicherzustellen das die Zahl überall geändert wurde. Dies ersparrt man sich mit einer dynamischen referenz. Normale variablen können zur laufzeit geändert werden und könnten somit zu memory leaks führen. Daher sollte man imutable Konstante verwenden.

### In Ihrem C-Programm sind zwei Arrays a und b definiert. Beide Arrays haben die Größe 10. Wie können Sie das Array a in das Array b kopieren?Wieso funktioniert das nicht mit b = a?
```c
strncpy(b, a, 10);
```
``b = a`` würde die addressen der arrays manipulieren und nicht deren werte.