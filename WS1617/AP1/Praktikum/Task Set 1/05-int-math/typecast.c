#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int a, b;

    printf("Programm zur Berechnung von Summe, Differenz, Produkt und Quotient\n");

    printf("Zwei ganze Zahlen eingeben: ");
    scanf("%d %d", &a, &b);

    printf("Summe: %d\n", a+b);
    printf("Differenz: %d\n", abs(a-b));
    printf("Produkt: %d\n",  a*b);

    // Durch den typecast in ein double lässt sich der quotient richtig als
    // fließkommazahl ausgeben
    printf("Quotient: %f\n",  (double) a/b);

    return 0;
}
