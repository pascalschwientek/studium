# Übungsblatt 2

## Aufgabe 10

Schreiben Sie ein Java-Programm, das einen Nettobetrag von der Tastatur einliest, die Mehrwertsteuer mit 19 % und den Bruttobetrag berechnet und alle 3 Werte 8-stellig mit 2 Nachkommastellen formatiert ausgibt.

Die Ausgabe des Programms sollte in etwa so aussehen:
```
Programm zur Berechnung eines Bruttobetrages

Bitte geben Sie den Nettobetrag in EUR ein: ___________

Nettobetrag                       =         xxxxx.xx EUR
Mehrwertsteuersatz 19.00 %        =         xxxxx.xx EUR
Bruttobetrag                      =         xxxxx.xx EUR
```

[Source Code](10-tax-calc/main.java)

## Aufgabe 11

Schreiben Sie ein Java-Programm, das die Anzahl der Sekunden im Monat November 2016 berechnet.

[Source Code](11-month-sec/main.java)

## Aufgabe 12

Schreiben Sie ein Java-Programm, das zur Berechnung von Rechtecken dient und die Seiten a und b in cm über die Tastatur einliest. Bitte geben Sie die eingelesenen Werte a und b, den Umfang sowie die Fläche des Rechteckes 8-stellig mit 2 Nachkommastellen formatiert aus.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm zur Berechnung des Umfanges und der Fläche von Rechtecken

Bitte geben Sie die Seite a in cm ein: __________
Bitte geben Sie die Seite b in cm ein: __________

Die Seite a ist xxxxx.xx cm lang.
Die Seite b ist xxxxx.xx cm lang.

Der Umfang des Rechtecks beträgt xxxxx.xx cm.
Die Fläche des Rechtecks beträgt xxxxx.xx cm².
```

[Source Code](12-area-calc/main.java)

## Aufgabe 13

Schreiben Sie ein Java-Programm, das die Oberfläche und das Volumen einer Kugel nach folgenden Formeln berechnet:

    V = (4*pi*r^3) / 3
    O = 4*pi*r^2

Die Ausgabe des Programms sollte in etwa so aussehen:
```
Programm Kugelberechnung

Bitte geben Sie den Radius der Kugel in cm ein: _________

Das Volumen für die Kugel ist xxxx.xx cm³.
       Die Oberfläche beträgt xxxx.xx cm².
```

[Source Code](13-vol-calc/main.java)

## Aufgabe 14

Schreiben Sie zwei kleine Java-Programme, die jeweils zwei Integer-Zahlen einlesen und anschließend deren Summe, Differenz, Produkt und Quotient ausgibt. **Was fällt Ihnen dabei auf?**

[Source Code](14-int-math/main.java)

Im zweiten Programm lösen Sie nun das Problem durch ein **type casting** und erläutern Sie bitte die Lösung.

[Source Code](14-int-math/main_b.java)

**Antwort**
Durch den typecast in einen double lässt sich der quotient richtig als fließkommazhal ausgeben.

## Aufgabe 15

    V = (L ⋅ 100) / K

Schreiben Sie ein Java-Programm, das den Benzinverbrauch eines Autos nach folgender Formel berechnet:

Einzulesen sind: Die verbrauchte Benzinmenge L in Litern und die zurückgelegte Wegstrecke K in Kilometern.

Die Ausgabe des Programms sollte in etwa so aussehen:
```
Programm zur Berechnung des Benzinverbrauchs eines Autos

Bitte geben Sie die verbrauchte Benzinmenge in Liter ein: ______
            Bitte geben Sie die gefahrenen Kilometer ein: ______

Ihr Wagen verbraucht xx.xx Liter Benzin auf 100 Kilometer.
```

[Source Code](15-car-usage/main.java)

## Aufgabe 16

Ein Unternehmen gewährt seinen Angestellten unter folgenden Bedingungen eine Jahresprämie.

- Beträgt das Dienstalter (=Anzahl der Dienstjahre) weniger als ein Jahr, wird keine Prämie gegeben
- Beträgt es mindestens ein Jahr aber weniger als sechs Jahre, besteht die Prämie aus 600 €.
- Ist der Mitarbeiter sechs oder mehr Jahre bei der Firma, bekommt er 250 € und dazu für jedes Dienstjahr 75 €.
- Ist er im letzteren Fall außerdem älter als 50 Jahre, so erhält er noch eine Zulage von 250 €.

Eingabe: Zuerst das Alter und anschließend das Dienstalter (ganze Jahre)

**Hinweis:** Verwenden Sie die if-Anweisung.

[Source Code](16-corp-bonus/main.java)

## Aufgabe 17

Schreiben Sie ein Java-Programm, das einen einfachen Taschenrechner simuliert. Lesen Sie zuerst die gewünschte Operation (+,-,\*,/) als Character-Zeichen ein und danach die beiden Operanden. Überprüfen Sie, ob ein gültiges Operationssymbol vorliegt. Wenn nicht, wiederholen Sie die Eingabe. Verhindern Sie eine Division durch Null, indem Sie eine entsprechende Fehlermeldung ausgeben und das Programm beenden.

**Hinweis:** Verwenden Sie die switch-Anweisung einschließlich default.

[Source Code](17-calc-simulator/main.java)

## Aufgabe 18

Beantworten Sie die folgenden Fragen schriftlich:

### Legen Sie ein Directory „Praktikum 2“ an und kopieren Sie alle erstellten Java-Programme dieses zweiten Praktikumblatts in dieses Directory. Wechseln Sie nun in dieses Directory und listen Sie die Dateinamen aller Java-Programme auf, die Sie entwickelt haben. Wie heißt hierzu die Befehlsfolge?
```bash
mkdir Praktikum2
cp code Praktikum2/code
cd Praktikum2
ls
```

### Übersetzen Sie nun das Programm aus Aufgabe 10 und führen Sie dieses Programm aus. Wie heißt die Befehlsfolge?
```bash
javac main.java main.class
java main
```

``Javac`` ist der Compiler-Aufruf. ``main.java`` ist die Quellcode-Datei, ``main.class`` ist die erzeugte Bytecode Datei, die von der Java Virtual Machine (JVM) interpretiert werden kann. 

### Schreiben Sie 5 Sätze zur Entwicklungsgeschichte von Java.
Die Urversion von Java wurde in einem Zeitraum von 18 Monaten vom Frühjahr 1991 bis Sommer 1992 unter dem Namen The Green Project von Patrick Naughton, Mike Sheridan, James Gosling und Bill Joy sowie neun weiteren Entwicklern im Auftrag des US-amerikanischen Computerherstellers Sun Microsystems entwickelt. Das Ziel war nicht nur die Entwicklung einer weiteren Programmiersprache, sondern einer vollständigen Betriebssystemumgebung, inklusive virtueller CPU, für unterschiedlichste Einsatzzwecke. Im März 1995 wurde die erste Alphaversion (1.0a2) des Java-Quellcodes für die Öffentlichkeit freigegeben. Wenig später, am 23. Mai 1995, wurde Java erstmals offiziell der Öffentlichkeit vorgestellt, in den „San Jose Mercury News“. Der Durchbruch kam mit der Integration von Java in den Browser Netscape Navigator.

### Was bedeutet Plattformunabhängigkeit und welche Rolle spielt hier der Bytecode? Schreiben Sie hierzu 5 Sätze und argumentieren Sie mit einem Beispiel.
Plattformunabhänging bedeutet das es dem Programm egal ist auf welcher grundlegenden Infrastruktur es läuft da es in einer darüberliegenden, abstrakteren umgebung ausgeführt wird.

### Was bedeutet Unicode? Schreiben Sie hierzu 5 Sätze.
Unicode ist ein internationaler Standard, in dem langfristig für jedes Sinn tragende Schriftzeichen oder Textelement aller bekannten Schriftkulturen und Zeichensysteme ein digitaler Code festgelegt wird. Ziel ist es, die Verwendung unterschiedlicher und inkompatibler Kodierungen in verschiedenen Ländern oder Kulturkreisen zu beseitigen. Unicode wird ständig um Zeichen weiterer Schriftsysteme ergänzt.

### Wie definiert man Konstanten in Java? Geben Sie 2 Beispiele an.
```java
public static final TYPE NAME = VALUE;
private static final TYPE NAME = VALUE;
```
### Wo findet man im Netz die Java API? Geben Sie eine mögliche URL an.
[https://docs.oracle.com/javase/7/docs/api/](https://docs.oracle.com/javase/7/docs/api/)

### Wie findet man in der API die Klasse Scanner? Wie viele Methoden hat diese Klasse ungefähr?
Unter java.util → Class Summary → Scanner.
Es gibt rund 50 Methoden der Klasse Scanner. 

### Erläutern Sie: 
```java
Scanner sc = new Scanner(System.in);
int i = sc.nextInt();
```

Scannt das nächste Token der Eingabe als int.
