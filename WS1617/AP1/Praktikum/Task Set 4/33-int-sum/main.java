import java.util.Scanner;

public class main
{
    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        int a, b;

        System.out.println("Programm Bereichsüberschreitungen");

        System.out.print("Geben Sie bitte zwei Integerzahlen ein: ");
        a = STDIN_SCANNER.nextInt();
        b = STDIN_SCANNER.nextInt();

        System.out.println("Die Summe von " + a + " und " + b + " ist " + (a + b));
    }
}