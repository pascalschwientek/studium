package de.plsk.factorysimulation.ui;

import de.plsk.factorysimulation.Machine;
import de.plsk.factorysimulation.Processor;
import de.plsk.factorysimulation.Producer;
import de.plsk.factorysimulation.Product;
import de.plsk.factorysimulation.comparators.NameComparator;
import de.plsk.factorysimulation.comparators.ProcedureComparator;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Iterator;

public class InputPanel extends JPanel {
    private PanelManager panelManager;

    protected JTextField productNameTextField  = new JTextField();
    protected JTextField productCostTextField  = new JTextField();
    protected JTextField productValueTextField = new JTextField();
    protected JButton    productCreateBtn      = new JButton("Produkt anlegen");

    protected JTextField machineNameTextField     = new JTextField();
    protected JTextField machineCostTextField     = new JTextField();
    protected JComboBox  producingComboBox        = new JComboBox<String>();
    protected JComboBox  dependencyComboBox       = new JComboBox<String>();
    protected JTextField dependencyCountTextField = new JTextField();
    protected JCheckBox  machineHasDependency     = new JCheckBox("Abhängigkeit festlegen");
    protected JButton    machineCreateBtn         = new JButton("Maschine anlegen");

    protected JLabel     testcreditLabel     = new JLabel("0");
    protected JTextField roundCountTextField = new JTextField();
    protected JButton    restFactoryBtn      = new JButton("Fabrik zurücksetzen");
    protected JButton    startSimulationBtn  = new JButton("Test starten");

    protected JLabel     titleSort               = new JLabel("Sort");
    protected JButton    machineSortNameBtn      = new JButton("by Name");
    protected JButton    machineSortProcedureBtn = new JButton("by Procedure");

    InputPanel(PanelManager panelManager) {
        super();
        this.panelManager = panelManager;

        this.setLayout(new GridLayout(0, 2));
        this.setBorder(new EmptyBorder(10, 10, 10, 10));

        JLabel titleProduct = new JLabel("Produkt");
        titleProduct.setFont(new Font("San Serif", Font.PLAIN, 18));

        JLabel titleMachine = new JLabel("Maschine");
        titleProduct.setFont(new Font("San Serif", Font.PLAIN, 18));

        JLabel titleFactory = new JLabel("Fabriktests");
        titleProduct.setFont(new Font("San Serif", Font.PLAIN, 18));

        productCreateBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                panelManager.addOrReplaceProductdraft(new Product(
                        productNameTextField.getText(),
                        Double.valueOf(productCostTextField.getText()),
                        Double.valueOf(productValueTextField.getText())
                ));

                productNameTextField.setText("");
                productCostTextField.setText("");
                productValueTextField.setText("");
            }
        });

//        machineNameTextField.addFocusListener(new FocusListener() {
//            @Override
//            public void focusGained(FocusEvent e) {
//                Iterator<Machine> iterator = panelManager.getMachinesDraft().iterator();
//
//                int index = 0;
//
//                while(iterator.hasNext()) {
//                    Machine tmp = iterator.next();
//
//                    if(tmp.getName().compareTo(machineNameTextField.getText()) == 0) {
//                        machineNameTextField.setBackground(Color.GREEN);
//                        break;
//                    }
//                    index++;
//                }
//                repaint();
//            }
//
//            @Override
//            public void focusLost(FocusEvent e) {
//                machineNameTextField.setBackground(Color.WHITE);
//                repaint();
//            }
//        });

        machineSortNameBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                panelManager.sort(new NameComparator());
            }
        });

        machineSortProcedureBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                panelManager.sort(new ProcedureComparator());
            }
        });

        machineCreateBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                Machine machine;

                if(machineHasDependency.isSelected()) {
                    machine = new Processor(
                            machineNameTextField.getText(),
                            Double.valueOf(machineCostTextField.getText()),
                            panelManager.getProductsDraft().get(producingComboBox.getSelectedIndex()),
                            panelManager.getProductsDraft().get(dependencyComboBox.getSelectedIndex()),
                            Integer.valueOf(dependencyCountTextField.getText())

                    );
                } else {
                    machine =  new Producer(
                            machineNameTextField.getText(),
                            Double.valueOf(machineCostTextField.getText()),
                            panelManager.getProductsDraft().get(producingComboBox.getSelectedIndex())
                    );
                }

                panelManager.addOrReplaceMachinedraft(machine);

                machineNameTextField.setText("");
                machineCostTextField.setText("");
                dependencyCountTextField.setText("");
            }
        });

        restFactoryBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                panelManager.resetFactory();
            }
        });

        startSimulationBtn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                panelManager.simulate(Integer.valueOf(roundCountTextField.getText()));

                testcreditLabel.setText(String.valueOf(panelManager.getTestcredit()));
            }
        });

        testcreditLabel.setText(String.valueOf(panelManager.getTestcredit()));

        this.add(titleProduct);                                 this.add(new JLabel());
        this.add(new JLabel("Name des Produktes"));             this.add(productNameTextField);
        this.add(new JLabel("Produktkosten"));                  this.add(productCostTextField);
        this.add(new JLabel("Verkaufswert"));                   this.add(productValueTextField);
        this.add(new JLabel());                                 this.add(productCreateBtn);

        this.add(titleMachine);                                 this.add(new JLabel());
        this.add(new JLabel("Name"));                           this.add(machineNameTextField);
        this.add(new JLabel("Kosten"));                         this.add(machineCostTextField);
        this.add(new JLabel("Erzeugnis"));                      this.add(producingComboBox);
        this.add(machineHasDependency);                         this.add(dependencyComboBox);
        this.add(new JLabel("Anzahl"));                         this.add(dependencyCountTextField);
        this.add(new JLabel());                                 this.add(machineCreateBtn);

        this.add(titleSort);                                    this.add(new JLabel());
        this.add(machineSortNameBtn);                           this.add(machineSortProcedureBtn);

        this.add(titleFactory);                                 this.add(new JLabel());
        this.add(new JLabel("Testguthabens"));                  this.add(testcreditLabel);
        this.add(new JLabel("Anzahl der Testrunden"));          this.add(roundCountTextField);
        this.add(restFactoryBtn);                               this.add(startSimulationBtn);
    }

    public void updateData() {
        producingComboBox.removeAllItems();
        dependencyComboBox.removeAllItems();

        for(int i = 0; i < panelManager.getProductsDraft().size(); i++) {
            Product product = panelManager.getProductsDraft().get(i);

            producingComboBox.addItem(product.getName());
            dependencyComboBox.addItem(product.getName());
        }

        testcreditLabel.setText(String.valueOf(panelManager.getTestcredit()));
    }
}
