import java.util.Scanner;
import java.lang.Math;

public class main
{
    private static Scanner in;

    public static void main(String[] args)
    {
        int wert1;
        int wert2;
        double summe;
        double differenz;
        double produkt;
        double quotient;

        System.out.println("Bitte geben Sie 2 Zahlen ein: ");
        Scanner a = new Scanner(System.in);
        wert1 = a.nextInt();
        a.close();

        Scanner b = new Scanner(System.in);
        wert2 = b.nextInt();
        b.close();

        summe = wert1 + wert2;
        differenz = wert1 - wert2;
        produkt = wert1 * wert2;
        quotient = wert1 / wert2;

        System.out.println("Die Summe von "+ wert1 +" und "+ wert2 +" ist "+ summe + ".\n");
        System.out.println("Die Differenz von "+ wert1 +" und "+ wert2 +" ist "+ differenz + ".\n");
        System.out.println("Das Produkt von "+ wert1 +" und "+ wert2 +" ist "+ produkt + ".\n");
        System.out.println("Der Quotient von "+ wert1 +" und "+ wert2 +" ist "+ quotient + ".\n");
    }
}