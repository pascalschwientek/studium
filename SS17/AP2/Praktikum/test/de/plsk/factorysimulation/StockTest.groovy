package de.plsk.factorysimulation

class StockTest extends GroovyTestCase {
    void testAdd() {
        Product product = new Product("Fidget Spinner", 1.00, 10.00)

        Stock stock = new Stock()

        stock.add(product)

        assertEquals(1, stock.count("Fidget Spinner"))
    }

    void testCount() {
        Product product = new Product("Fidget Spinner", 1.00, 10.00)
        Product other = new Product("Smartphone", 100.00, 150.00)

        Stock stock = new Stock()

        stock.add(product)
        stock.add(product)
        stock.add(other)

        assertEquals(2, stock.count("Fidget Spinner"))
        assertEquals(1, stock.count("Smartphone"))
    }

    void testRemove() {
        Product product = new Product("Fidget Spinner", 1.00, 10.00)

        Stock stock = new Stock()

        stock.add(product)
        stock.add(product)

        stock.remove("Fidget Spinner")
        assertEquals(1, stock.count("Fidget Spinner"))
    }

    void testRemove1() {
        Product product = new Product("Fidget Spinner", 1.00, 10.00)

        Stock stock = new Stock()

        stock.add(product)
        stock.add(product)
        stock.add(product)
        stock.add(product)

        stock.remove("Fidget Spinner", 2)
        assertEquals(2, stock.count("Fidget Spinner"))
    }

    void testSell() {
        Product product = new Product("Fidget Spinner", 1.00, 10.01)

        Stock stock = new Stock()

        stock.add(product)
        stock.add(product)
        stock.add(product)
        stock.add(product)


        assertEquals(40.04, stock.sell())
        assertEquals(0, stock.count("Fidget Spinner"))
    }
}
