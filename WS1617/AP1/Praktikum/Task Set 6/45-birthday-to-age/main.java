import java.util.Scanner;

public class main
{
    public final static int NOW = 2017;

    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static int birthyearInput() {
        int birthyear;

        System.out.print("Bitte geben Sie Ihr Geburtsjahr ein: ");
        birthyear = STDIN_SCANNER.nextInt();
        STDIN_SCANNER.close();

        return birthyear;
    }

    public static int ageCalc(int birthyear) {
        return NOW - birthyear;
    }

    public static int ageOutput(int age) {
        System.out.print("Sie sind heute " + Integer.toString(age) + " Jahre alt.");

        return 0;
    }

    public static void main(String[] args) {
        System.out.println("Programm zur Berechnung eines Alters einer Person");

        System.exit(ageOutput(ageCalc(birthyearInput())));
    }
}