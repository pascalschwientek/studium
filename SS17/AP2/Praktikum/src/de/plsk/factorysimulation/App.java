package de.plsk.factorysimulation;

public class App {

    /**
     * TODO javadoc
     */
    public static void Main() {
        Factory factory = new Factory("ChinaCopyCat");

        factory.addMachine(new Producer("PlasticPress", 10.0, new Product("Fidget Spinner", 5, 50)));
        factory.addMachine(new Producer("PlasticPress", 10.0, new Product("Fidget Spinner", 5, 50)));
        factory.addMachine(new Producer("PlasticPress", 10.0, new Product("Fidget Spinner", 5, 50)));

        factory.testcredit = 50;

        Double credit = factory.simulate(10);

        System.out.printf("The Credit after 10 rounds: %f", credit);
    }
}
