#include <stdio.h>
#include <math.h>

int main(void)
{
    float x,y;

    printf("Programm zur Berechnung der Potenz\n");

    printf("Bitte geben Sie x ein: ");
    scanf("%f", &x);

    printf("Bitte geben Sie y ein: ");
    scanf("%f", &y);

    printf("Die Potenz von x hoch y ist: %f\n", pow(x,y));

    return 0;
}
