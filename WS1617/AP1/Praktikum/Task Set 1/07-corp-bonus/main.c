#include <stdio.h>

int main(void)
{
    int age, corpage;
    int bonus = 0;

    printf("Programm zur Überprüfung der verfügbarkeit einer Jahresprämie\n");

    printf("\nBitte geben Sie ihr Alter ein: ");
    scanf("%d", &age);

    printf("\nBitte geben Sie ihr Dienstalter ein: ");
    scanf("%d", &corpage);

    if(corpage < 1) {
        printf("\nKeine Jahresprämie verfügbar: ");

        return 0;
    }

    if(corpage >= 1 && corpage < 6) {
        printf("\nIhre Jahresprämie Beträgt: 600 EUR");

        return 0;
    }

    if(age > 50)
    {
        bonus = bonus + 250;
    }

    bonus = bonus + 250 + (corpage * 75);

    printf("Ihre Jahresprämie Beträgt: %d EUR", bonus);

    return 0;
}
