import java.util.Scanner;

public class main
{
    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        float x, y;
        double power;

        System.out.println("Programm zur Berechnung der Potenz");

        System.out.print("Bitte geben Sie x ein: ");
        x = STDIN_SCANNER.nextFloat();

        System.out.print("Bitte geben Sie y ein: ");
        y = STDIN_SCANNER.nextFloat();

        STDIN_SCANNER.close();
        power = Math.pow(x, y);

        System.out.printf("Die Potenz von x hoch y ist: %f\n", power);
    }
}