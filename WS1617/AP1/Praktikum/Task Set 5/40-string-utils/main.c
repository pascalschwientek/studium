#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stringFunctions.h"

//gcc -std=c99 -o main main.c stringFunctions.c -lm

int main(void)
{
    char a[254], b[254];
    int isEqual;

    printf("Programm zum Verbinden von Freunden\n");

    printf("Bitte geben Sie den 1. Vornamen ein: ");
    if (!(fgets(a, sizeof a, stdin) != NULL)) {
        fprintf(stderr, "Error reading Name.\n");
        exit(1);
    }

    printf("Bitte geben Sie den 2. Vornamen ein: ");
    if (!(fgets(b, sizeof b, stdin) != NULL)) {
        fprintf(stderr, "Error reading Name.\n");
        exit(1);
    }

    strtok(a, "\n");
    strtok(b, "\n");

    isEqual = equal(a,b);

    if(isEqual) {
        printf("Status: %s hat keine Freunde.\n", a);
    } else {
        stringconcatenate(a, b);
        printf("Status: %s sind Freunde.\n", a);
    }

    return 0;
}
