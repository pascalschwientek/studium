#include <stdio.h>

int main(void)
{
    float gasoline, distance, usage;

    printf("Programm zur Berechnung des Benzinverbrauchs eines Autos\n");

    printf("\nBitte geben Sie die verbrauchte Benzinmenge in Liter ein: ");
    scanf("%f", &gasoline);

    printf("\nBitte geben Sie die gefahrenen Kilometer ein: ");
    scanf("%f", &distance);

    usage = (gasoline * 100) / distance;

    printf("Ihr Wagen verbraucht %4.2f Liter Benzin auf 100 Kilometer.\n", usage);
}
