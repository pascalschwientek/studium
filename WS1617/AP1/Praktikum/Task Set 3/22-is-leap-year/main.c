#include <stdio.h>

int main(void)
{
    int year;

    printf("Programm Schaltjahr\n");

    printf("Bitte geben Sie eine Jahreszahl ein: ");
    scanf("%d", &year);

    if(year%400 == 0) {
        printf("%d ist ein Schaltjahr!\n", year);
    } else if(year%100 == 0) {
        printf("%d ist kein Schaltjahr!\n", year);
    } else if(year%4 == 0) {
        printf("%d ist ein Schaltjahr!\n", year);
    } else {
        printf("%d ist kein Schaltjahr!\n", year);
    }

    return 0;
}
