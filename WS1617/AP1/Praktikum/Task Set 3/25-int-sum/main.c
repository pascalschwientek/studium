#include <stdio.h>

int main(void)
{
    int a, b;

    printf("Programm Bereichsüberschreitungen\n");

    printf("Geben Sie bitte zwei Integerzahlen ein: ");
    scanf("%d %d", &a, &b);

    printf("Die Summe von %d und %d ist %d\n", a, b, a+b);
}
