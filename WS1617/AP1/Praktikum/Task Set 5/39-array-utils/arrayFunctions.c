#include <stdlib.h>
#include "arrayFunctions.h"

int compare (const void * a, const void * b)
{
    return (*(int*)a - *(int*)b);
}

float mean(int numbers[], int size){
    float median;

    qsort(numbers, size, sizeof(int), compare);

    if(size%2 == 0)
    {
        median = numbers[(size/2)-1] + numbers[size/2];
        median = median / 2;
    } else {
        median = numbers[size/2];
    }

    return median;
}

int biggest(int numbers[], int size){
    qsort(numbers, size, sizeof(int), compare);

    return numbers[size-1];
}

int smallest(int numbers[], int size){
    qsort(numbers, size, sizeof(int), compare);

    return numbers[0];
}

int secondSmallest(int numbers[], int size){
    qsort(numbers, size, sizeof(int), compare);

    return numbers[1];
}