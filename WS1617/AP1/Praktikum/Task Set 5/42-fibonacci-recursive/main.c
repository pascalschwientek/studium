#include <stdio.h>

int fibonacci(int n)
{
    if(n == 0){
        return 0;
    }else if(n == 1){
        return 1;
    }else{
        return (fibonacci(n-1) + fibonacci(n-2));
    }
}

int main(void)
{
    int n, i, c = 0;

    printf("Fibonacci Folge (Rec)\n");

    printf("\nLänge der Fibonacci Folge: ");
    scanf("%d",&n);

    printf("Die ersten %d Ziffern der Fibonacci Folge: \n",n);

    for (i=1;i<=n;i++)
    {
        printf("%d\n", fibonacci(c));
        c++;
    }

    return 0;
}
