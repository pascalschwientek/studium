package de.plsk.factorysimulation

class AppTest extends GroovyTestCase {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream()
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream()

    void setUp() {
        super.setUp()

        System.setOut(new PrintStream(outContent))
        System.setErr(new PrintStream(errContent))
    }

    void tearDown() {
        System.setOut(null)
        System.setErr(null)
    }

    void testMain() {
        App.Main()
    }
}
