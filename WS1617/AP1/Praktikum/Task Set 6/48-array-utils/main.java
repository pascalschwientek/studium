import java.util.Scanner;

public class main
{
    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static int[] einlesen(){
        int count;
        int[] array;

        System.out.print("Wie viele Werte sollen eingelsen werden?: ");
        count = STDIN_SCANNER.nextInt();

        array = new int[count];

        for(int i = 0; i < count; i++) {
            System.out.print("Bitte geben Sie den " + (i + 1) + ". Wert ein: ");
            array[i] = STDIN_SCANNER.nextInt();
        }

        return array;
    }

    public static void ausgeben(int[] refArray){
        System.out.println("Ausgabe: ");

        for (int i = 0; i < refArray.length; i++) {
            System.out.print(refArray[i] + ", ");
        }

        System.out.println("\n");
    }

    public static double mittelwert(int[] refArray){
        double summe = 0;

        for (int i = 0; i < refArray.length; i++) {
            summe += refArray[i];
        }

        return summe / refArray.length;
    }

    public static int groesste(int[] refArray){
        int maxValue = refArray[0];

        for(int i = 1; i < refArray.length; i++){
            if(refArray[i] > maxValue){
                maxValue = refArray[i];
            }
        }

        return maxValue;
    }

    public static int kleinste(int[] refArray){
        int minValue = refArray[0];

        for(int i = 1; i < refArray.length; i++){
            if(refArray[i] < minValue){
                minValue = refArray[i];
            }
        }

        return minValue;
    }

    public static int zweitkleinste(int[] refArray){
        int smallest = Integer.MAX_VALUE, secondSmallest = Integer.MAX_VALUE;

        for (int i = 0; i < refArray.length; i++) {
            if (refArray[i] < smallest) {
                secondSmallest = smallest;
                smallest = refArray[i];
            } else if (refArray[i] < secondSmallest) {
                secondSmallest = refArray[i];
            }
        }

        return secondSmallest;

    }

    public static int[] reverseabsolute(int[] refArray){
        int[] reverseAbs = refArray;

        for (int i = 0; i < refArray.length; i++) {
            reverseAbs[refArray.length - 1 - i] = Math.abs(refArray[i]);
        }

        return reverseAbs;
    }

    public static void main(String[] args) {
        System.out.println("Programm Array Utilities");

        int[] numbers = einlesen(), rev_abs;
        ausgeben(numbers);

        int big, small, small_sec;
        double mittel;

        mittel = mittelwert(numbers);
        big = groesste(numbers);
        small = kleinste(numbers);
        small_sec = zweitkleinste(numbers);
        rev_abs = reverseabsolute(numbers);

        System.out.println("Mittelwert   :" + mittel);
        System.out.println("Groesstes    :" + big);
        System.out.println("Kleinstes    :" + small);
        System.out.println("2. Kleinstes :" + small_sec);
    }
}