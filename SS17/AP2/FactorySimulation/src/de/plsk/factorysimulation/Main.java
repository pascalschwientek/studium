package de.plsk.factorysimulation;

import de.plsk.factorysimulation.ui.PanelManager;

import javax.swing.*;

public class Main {

    /**
     * TODO javadoc
     */
    public static void main(String[] args) {
        Factory factory = new Factory("ChinaCopyCat", 50);

        PanelManager panelManager = new PanelManager(factory);
        panelManager.start();
    }
}
