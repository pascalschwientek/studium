#include <stdio.h>
#include <stdlib.h>
#include "arrayFunctions.h"

int main(void)
{
    int input, b, s, s2, i = 0;
    float m;
    int array[1000], revAbs[1000];

    printf("\nZahlen hinzufügen (0 zum stoppen): ");

    do {
        scanf("%d", &array[i]);
        i++;
    } while(array[i-1] != 0);

    m = mean(array, i-1);
    b = biggest(array, i-1);
    s = smallest(array, i-1);
    s2= secondSmallest(array, i-1);

    printf("Mittelwert.........: %f\n", m);
    printf("Größte Zahl........: %d\n", b);
    printf("Kleinste Zahl......: %d\n", s);
    printf("2. Kleinste Zahl...: %d\n", s2);


    for (int j = 0; j < i-1; j++) {
        revAbs[i-1-1-j] = abs(array[i-1]);
    }

    return 0;
}
