# Übungsblatt 1

## Aufgabe 1

Schreiben Sie ein C-Programm, das einen Nettobetrag von der Tastatur einliest, die Mehrwertsteuer mit 19 % und den Bruttobetrag berechnet und alle 3 Werte 8-stellig mit 2 Nachkommastellen formatiert ausgibt.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm zur Berechnung eines Bruttobetrages

Bitte geben Sie den Nettobetrag in EUR ein: ___________

Nettobetrag                       =         xxxxx.xx EUR
Mehrwertsteuersatz 19.00 %        =         xxxxx.xx EUR
Bruttobetrag                      =         xxxxx.xx EUR
```

[Source Code](01-tax-calc/main.c)

## Aufgabe 2

Schreiben Sie ein C-Programm, das die Anzahl der Sekunden im Monat November 2016 berechnet.

[Source Code](02-month-sec/main.c)

## Aufgabe 3

Schreiben Sie ein C-Programm, das zur Berechnung von Rechtecken dient und die Seiten a und b in cm über die Tastatur einliest. Bitte geben Sie die eingelesenen Werte a und b, den Umfang sowie die Fläche des des Rechteckes 8-stellig mit 2 Nachkommastellen formatiert aus.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm zur Berechnung des Umfanges und der Fläche von Rechtecken

Bitte geben Sie die Seite a in cm ein: _________
Bitte geben Sie die Seite b in cm ein: _________

Die Seite a ist xxxxx.xx cm lang.
Die Seite b ist xxxxx.xx cm lang.
Der Umfang des Rechtecks beträgt xxxxx.xx cm.
Die Fäche  des Rechtecks beträgt xxxxx.xx cm².
```

[Source Code](03-area-calc/main.c)

## Aufgabe 4

Schreiben Sie ein C-Programm, das die Oberfläche und das Volumen einer Kugel nach folgenden Formeln berechnet:

    V = (4*pi*r^3) / 3
    O = 4*pi*r^2

Die Ausgabe des Programms sollte in etwa so aussehen:
```
Programm Kugelberechnung

Bitte geben Sie den Radius der Kugel in cm ein: _________

Das Volumen für die Kugel ist xxxx.xx cm³.
       Die Oberfläche beträgt xxxx.xx cm².
```

[Source Code](04-vol-cal/main.c)

## Aufgabe 5

Schreiben Sie zwei kleine C-Programme, die jeweils zwei Integer-Zahlen einlesen und anschließend deren Summe, Differenz, Produkt und Quotient ausgibt. **Was fällt Ihnen dabei auf?**

[Source Code](05-int-math/main.c)

Im zweiten Programm lösen Sie nun das Problem durch ein **type casting** und erläutern Sie bitte die Lösung.

[Source Code](05-int-math/typecast.c)

**Antwort**
Durch den typecast in einen double lässt sich der quotient richtig als fließkommazhal ausgeben.

## Aufgabe 6

Schreiben Sie ein C-Programm, das den Benzinverbrauch eines Autos nach folgender Formel berechnet:

    V = (L ⋅ 100) / K
    
Einzulesen sind: Die verbrauchte Benzinmenge L in Litern und die zurückgelegte Wegstrecke K in Kilometern.

Die Ausgabe des Programms sollte in etwa so aussehen:
```
Programm zur Berechnung des Benzinverbrauchs eines Autos

Bitte geben Sie die verbrauchte Benzinmenge in Liter ein: ______
            Bitte geben Sie die gefahrenen Kilometer ein: ______

Ihr Wagen verbraucht xx.xx Liter Benzin auf 100 Kilometer.
```

[Source Code](06-car-usage/main.c)

## Aufgabe 7

Ein Unternehmen gewährt seinen Angestellten unter folgenden Bedingungen eine Jahresprämie.

- Beträgt das Dienstalter (=Anzahl der Dienstjahre) weniger als ein Jahr, wird keine Prämie gegeben
- Beträgt es mindestens ein Jahr aber weniger als sechs Jahre, besteht die Prämie aus 600 €.
- Ist der Mitarbeiter sechs oder mehr Jahre bei der Firma, bekommt er 250 € und dazu für jedes Dienstjahr 75 €.
- Ist er im letzteren Fall außerdem älter als 50 Jahre, so erhält er noch eine Zulage von 250 €.

Eingabe: Zuerst das Alter und anschließend das Dienstalter (ganze Jahre)

**Hinweis:** Verwenden Sie die if-Anweisung.

[Source Code](07-corp-bonus/main.c)

## Aufgabe 8

Schreiben Sie ein C-Programm, das einen einfachen Taschenrechner simuliert. Lesen Sie zuerst die gewünschte Operation (+,-,\*,/) als Character-Zeichen ein und danach die beiden Operanden. Überprüfen Sie, ob ein gültiges Operationssysmbol vorliegt. Wenn nicht, wiederholen Sie die Eingabe. Verhindern Sie eine Division durch Null, indem Sie eine entsprechende Fehlermeldung ausgeben und das Programm beenden.

**Hinweis:** Verwenden Sie die switch-Anweisung einschließlich default.

[Source Code](08-calc-simulator/main.c)

## Aufgabe 9

Beantworten Sie die folgenden Fragen schriftlich:

### Was bewirken die UNIX-Kommandos pwd, ls, ps, man und who?

| **UNIX-Kommando** | **Funktion**                             |
| ----------------- | ---------------------------------------- |
|  **pwd**          | Gibt das aktuelle verzeichnis zurück     |
|  **ls**           | Listet den Verzeichnisinhalt             |
|  **ps**           | Prozess Status                           |
|  **man**          | Zeigt das “on-line” Handbuch             |
|  **who**          | Gibt den Namen des aktuellen Nutzers aus |

### Legen Sie ein Directory „Praktikum 1“ an und kopieren Sie alle erstellten Programme dieses ersten Praktikumblatts in dieses Directory. Wechseln Sie nun in dieses Directory und listen Sie die Dateinamen aller C-Programme auf, die Sie entwickelt haben. Wie heißt hierzu die Befehlsfolge?
```bash
mkdir Praktikum1
cp code Praktikum1/code
cd Praktikum1
ls
```

### Übersetzen Sie nun das Programm aus Aufgabe 1, indem Sie der ausführbaren Datei einen eigenen Namen geben (nicht a.out) und führen Sie dieses Programm mit diesem Namen aus. Wie heißt die Befehlsfolge?
```bash
gcc main.c -o main
./main
```

### Welchen Editor benutzen Sie und wie rufen Sie ihn auf?
Ich verwende den VIM Editor. Gestartet wird der VIM Editor mit dem Befehl ``vi`` und optional einer Datei als Argument. ``vi datei.txt``

### Was ist ein Algorithmus?
Ein Algorithmus ist eine ausführbare Folge von endlich vielen Anweisungen zur Lösung eines Problems oder einer Problemklasse. Ein Algorithmus ist in der Regel allgemeingültig. An jeder Stelle des Algorithmus muss eindeutig festgelegt sein, was zu tun ist und welcher Schritt der nächste ist. Wird ein Algorithmus mit den gleichen Eingabewerten und Startbedingungen wiederholt, so liefert er stets das gleiche Ergebnis.

### Geben Sie ein Beispiel für einen Algorithmus an.
1962 präsentierte Tony Hoare von der Elliott Brothers, Ltd., aus London den Quicksort Algorithmus. N Sachen in numerischer oder alphabetischer reihnolge möglichst effizent zu sortieren, dieses problem versucht der Quicksort Algortihmus zu lösen. Dazu arbeitet er sich mit einer Rekusiven Strategie der teilung durch das Datenset und erreicht so eine durchnittliche laufzeit von O(N log N).

### Was ist eine Datenstruktur?
Eine Datenstukrtur ist ein Object zur Speicherung von strukturierten Daten. Also Daten die in einer bestimmten weise angeordnet sind, um den zugriff auf diese zu ermöglichen.

### Wieso kann man sagen, dass ein Programm aus Algortithmus und Datenstruktur besteht? Erläutern Sie dies an dem Beispiel aus Aufgabe 1.
X

### Wer ist N. Wirth? Was sind seine entscheidenden Beiträge zur Informatik? Schreiben Sie hierzu 5 Sätze.
Niklaus Wirth, geboren 15. Februar 1934 in Wuterhur (CH) ist ein Informatiker und entwickler der Pascal Programmiersprache. Nach ihm benannt ist das Wirthsche Gesetz, nach dem sich die Software schneller verlangsamt als sich die Hardware beschleunigt. Enttäuscht über die stetig zunehmende Komplexität seiner bisher mitgewirkten Programmiersprache definierte und implementierte er in den Jahren 1968 bis 1972 praktisch im Alleingang die Programmiersprache Pascal. Wirth erhielt zahlreiche Ehrungen unter anderem. im Jahr 1984 den Turing Award als erster und bisher einziger deutschsprachiger Informatiker, sowie 1988 den IEEE Computer Pioneer Award.

### "as versteht man unter Interoperabilität?
Systeme die kompatible zueinander sind durch die einhalung gemeinsamer Standards kommunizieren bezeichnet man als Interoperatbil.

### Wieso ist der strukturierte Programmieransatz (prozedurale Programmierung) hilfreich für kleine Programme und für große Programmsysteme kritisch? Argumentieren Sie mit der Softwarekrise der 70-er Jahre und schreiben Sie hierzu 5 Sätze.
X

### Wer war Dennis M. Ritchie? Was waren seine entscheidenden Leistungen für die Informatik? Schreiben Sie hierzu 5 Sätze.
Dennis M. Ritchie, geboren 1941 in New York, gestorben am 12. Oktober 2011 in New Jersey, war ein Informatiker der ersten Stunde. Er entiwckelte zusammen mit Ken Thompson die erste UNIX Version. Gemeinsam mit Thompson und Brian Kernighan entwickelte er auserdem die Progammiersprache C. Zusammen mit Kernighan schrieb er im anschluss das Buch „The C Programming Language“. Für seine arbeiten am Unix Betriebsystem erhielt er den Turning-Award. Bekannt ist er unter vielen Informatikern als „dmr“, seinem Username für den UNIX System-Login.

### Erläutern Sie die Begriffe Programm, Syntax und Semantik.
Programm: Folge von Anweisungen (bestehend aus Deklarationen und Instruktionen), um bestimmte Funktionen zu auszuführen.

Syntax: Regelsystem zur Kombination elementarer Zeichen zu zusammengesetzten Zeichen in natürlichen oder künstlichen Zeichensystemen

Semantik: Die Semantik beschäftigt sich mit den Beziehungen zwischen den Zeichen und deren Bedeutungen.

### Aus welchen Teilen besteht eine Funktion in C?
Funktionsdefinition (Bestehnd aus Funktionstype, Funktionsbezeichner und Übergabeparameter), gefolgt von den Funktionsanweisungen.
```
Rückgabetyp  bezeichner(Übergabeparameter)
{
    Anweisungen
}
```

### Was ist ein Bezeichner?
"Bezeichner" oder "Symbole" sind die Namen, die Sie für Variablen, Typen, Funktionen und Beschriftungen im Programm angeben.

Quelle: C Sprachreferenz - https://msdn.microsoft.com/de-de/library/e7f8y25b.aspx

### Aus welchen Teilen besteht die Definition einer Variablen in C?
Aus dem Typen und einem Bezeichner, optional kann auch ein standartwert der Variable angebene werden. type bezeichner = standartwert

### Was ist eine Zeichenkonstante in C? Geben Sie auch ein Beispiel an.
Eine Zeichenkonstante besteht aus in Apostrophen eingeschlossene Zeichen, wie etwa `A`, `=` oder `\n`.

### Was ist ein side effect? Wieso sollte man side effects vermeiden?
```c
double d = 3.5;
int x, y;
y = (x = d) * 5; // y = 15
double z = x = d; // z 3 weil x type int
```

Der zurückgegebene Wert von x=d ist der main effect. Die änderung des werts von x ist ein side effect.

### Wie ist der Wert des folgenden Ausdrucks: a = b = 0 
    A=0
    B=0

### Beweisen Sie: ausdruck == 0 ist das gleiche wie !ausdruck.
    0 == false


