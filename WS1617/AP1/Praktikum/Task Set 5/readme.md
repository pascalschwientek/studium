# Übungsblatt 5

## Aufgabe 35

Schreiben Sie ein C-Programm, das für ein Geburtsjahr, das eingegeben wird, das Alter einer Person berechnet.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm zur Berechnung eines Alters einer Person

Bitte geben Sie Ihr Geburtsjahr ein: ___________

Sie sind heute ______ Jahre alt.
```

Implementieren Sie dieses Programm mit den folgenden Funktionen:

1. ```int eingabeGebJahr()```

    Hier wird die Person nach dem Geburtsjahr gefragt. Dieses wird als Returnwert zurückgegeben.

1. ```int alterBerechnung(int gebJahr)```

    Das Geburtsjahr wird als Parameter übergeben, das Alter wird berechnet und als Returnwert zurückgegeben.

1. ```void ausgabeAlter(int alter)```

    Das Alter wird als Parameter übergeben und die obige Ausgabe wird erzeugt.

1. ```int main()```

    Hier werden die obigen Funktionen aufgerufen.

[Source Code](35-birthday-to-age/main.c)

## Aufgabe 36

Schreiben Sie ein C-Programm, das x = y berechnet, wenn die Zahlen **x** und **y** eingegeben werden. Verwenden Sie hierzu die Funktion **pow** aus der mathematischen C-Bibliothek.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm zur Berechnung der Potenz

Bitte geben Sie x ein: ___________
Bitte geben Sie y ein: ___________

Die Potenz von x hoch y ist: _____
```

[Source Code](36-pow-calc/main.c)

## Aufgabe 37

Schreiben Sie ein Programm zur Ermittlung des größten gemeinsamen Teilers von zwei positiven Zahlen und zum Kürzen von Brüchen. Entwickeln Sie dazu als erstes die Funktion

```c
unsigned int ggt(unsigned int a, unsigned int b)
```

Der größte gemeinsame Teiler ist nach dem Euklidischen Algorithmus zu berechnen. Dieser geht davon aus, dass für zwei positive Zahlen **a** und **b** mit **a &gt;= b** gilt:

    ggt(a,b) = ggt(b, a % b)

Entwickeln Sie eine Schleife, die nach der angegebenen Grundidee die beiden Eingabegrößen **a** und **b** solange verkleinert, bis **b == 0** ist. In dem Fall ist **a** nämlich gleich dem gesuchten Funktionsresultat.

Die Funktion ```main()``` soll nach Zähler und Nenner fragen und dann die ggt-Funktion aufrufen, die den größten gemeinsamen Teiler berechnet. Dann soll der gekürzte Bruch ausgegeben werden.

Die Ausgabe des Programms sollte in etwa so aussehen:

```
Programm zum Kürzen von Brüchen

Bitte geben Sie den Zähler ein:         _________
Bitte geben Sie den Nenner ein:         _________

Der größte gemeinsame Teiler ist:       _________
Der gekürzte Bruch heißt:               _________
```

**Hinweis:** Benutzen Sie kein ```struct```.

[Source Code](37-shared-divider/main.c)

## Aufgabe 38

Lösen Sie Aufgabe 37 mit einem ```struct Bruch```, das als Komponenten ```zaehler``` **und** ```nenner``` hat.

[Source Code](38-shared-devider-struct/main.c)

## Aufgabe 39

Schreiben Sie ein Programm, das alles Mögliche mit Arrays macht. Formulieren Sie alle auszuführenden Aktionen 
als Funktionen und speichern Sie diese in einem separaten File ```arrayFunctions.c``` ab. 
Schreiben Sie die Funktionsdeklarationen in das Header-File ```arrayFunctions.h``` und führen Sie die Übersetzung des Programms mit ```make``` aus.

Das Hauptprogramm (in ```arrayMain.c``` ) soll folgende Aktionen nacheinander ausführen:

- Einlesen von Zahlen

    Es sollen solange ganze Zahlen eingelesen werden, bis eine Null eingegeben wird. Die Null soll nicht im Array gespeichert werden.

- Mittelwert der Zahlen ausgeben.
- Die größte Zahl ausgeben.
- Die kleinste Zahl ausgeben.
- Die zweitkleinste Zahl ausgeben.
- Die Zahlen in umgekehrter Reihenfolge und nur mit ihren Absolutbeträgen ausgeben.

Kopieren Sie hierzu die Absolutbeträge der Zahlen in umgekehrter Reihenfolge in ein anderes Array.

> Note: Ist hier eine Lösung mit dynamic allocated memory gefordert?

[Source Code](39-array-utils/main.c)

## Aufgabe 40

Schreiben Sie ein Programm, das alles Mögliche mit Zeichenketten (Strings) macht. Formulieren Sie alle auszuführenden 
Aktionen als Funktionen und speichern Sie diese in einem separaten File ```stringFunctions.c``` ab. 
Schreiben Sie die Funktionsdeklarationen in das Header-File ```stringFunctions.h``` und führen Sie die Übersetzung des Programms mit ```make``` aus.

Das Hauptprogramm (in ```stringMain.c``` ) soll folgende Aktionen nacheinander ausführen:

- Einlesen von zwei Vornamen.

    Legen Sie hierzu zwei **char** -arrays an.

- Vergleich der beiden Namen auf Gleichheit.

    Verwenden Sie die (selbst geschriebene) Funktion **int equal(char x[], char y[])**, wobei der Rückgabewert **1** ist, falls Gleichheit gilt, sonst **0**.

- Nur wenn die beiden Vornamen ungleich sind, sollen diese als Freunde verbunden werden. Verwenden Sie hierzu eine (selbst geschriebene) Funktion **stringconcatenate** , die zwei Strings als Eingabeparameter hat und einen dritten String erzeugt, der die beiden Strings durch ein **&amp;** -Zeichen verbunden enthält.

- Dann soll im Hauptprogramm folgendes ausgegeben werden:
    ```
    <vorname_1> & <vorname_2> sind Freunde. 
    <vorname_1> hat keine Freunde.
    ```

```
Programm zum Verbinden von Freunden

Bitte geben Sie den 1. Vornamen ein:         Luisa
Bitte geben Sie den 2. Vornamen ein:         Frank

Status: Luisa & Frank sind Freunde.
```

Oder:

```
Bitte geben Sie den 1. Vornamen ein:         Luisa
Bitte geben Sie den 2. Vornamen ein:         Luisa

Status: Luisa hat keine Freunde.
```

[Source Code](40-string-utils/main.c)

## Aufgabe 41

Schreiben Sie ein Programm zur Verwaltung eines Telefonverzeichnisses. Wir wollen hierzu ein **struct** verwenden, das die folgenden Komponenten hat:

```c
char name[laengeName];
char vorname[laengeName];
char vorwahl[laengeVorwahl];
char nummer[laengeNummer];
```

Das ```verzeichnis[anzahlEintraege]``` enthält dann als Array-Elemente Einträge der obigen Struktur.

Folgende Funktionen sollen geschrieben werden:

- Eintrag einfügen.

    Ein Eintrag soll in die Liste eingetragen werden, falls noch Platz ist und falls der Name noch nicht existiert.

- Eintrag löschen.

    Geben Sie einen Namen ein und löschen Sie den Eintrag, falls der Name existiert.

- Eintrag suchen.

    Für einen Namen soll die Telefonnummer ausgegeben werden.

- Schreiben Sie eine Menüsteuerung, durch die sich die einzelnen Funktionen aufrufen lassen.

**Hinweis:** Schreiben Sie alle Funktionen in das **main** -Programm. Sie sollen für dieses Beispiel kein **make** verwenden und keine separate Übersetzung.

[Source Code](41-phonebook/main.c)

## Aufgabe 42

Schreiben Sie eine **rekursive** -Funktion für die Fibonacci-Zahlen:

```
fn = fn-1 + fn-2 für n >=2 und 
f0 = 0 und f1 = 1, sonst.
```

Schreiben Sie auch eine **main** -Funktion, die eine Zahl von der Tastatur einliest und dann das Fibonacci-Ergebnis für diese Zahl ausgibt. Bitte testen Sie, ab wann der Berechnungsaufwand zu groß wird, so dass wir nicht auf das Ergebnis warten können.

[Source Code](42-fibonacci-recursive/main.c)

## Aufgabe 43

Schreiben Sie eine **iterative** -Funktion für die Fibonacci-Zahlen und eine **main** -Funktion, die eine Zahl von der Tastatur einliest und dann das Fibonacci-Ergebnis für diese Zahl ausgibt.

[Source Code](43-fibonacci-iterative/main.c)

## Aufgabe 44

Beantworten Sie die folgenden Fragen schriftlich:

### Diskutieren Sie den Aufwand für die rekursive und iterative Form der Fibonacci Funktion (Aufgabe 42 und 43).

Bei einer Rekursiven Funktion ist der Stack größer und es wird mehr Speicher benötigt.

### Beschreiben Sie kurz ein Anwendungsbeispiel aus der Praxis für die Fibonacci Zahlen.

Goldener Schnitt / Golden Ratio

### Was ist der Unterschied zwischen Call by Value und Call by Reference in der Sprache C?

Bei Call by Value wird der Wert übergeben. Bei Call by Reference wird eine Reference oder Pointer übergeben der an den
Speicherort der Variable zeigt.

#### Wann verwendet man diese beiden Übergabemethoden in Funktionen?

Wenn man den Value einer Variable selber verändern möchte sollte man CBR verwenden. Wird die Variable für Bedingungen verwendet ist CBV in Ordnung.

#### Was ist bei der Übergabe von Arrays zu beachten?

### Erläutern Sie bitte:
```c
int x = 50;
int *z = &x;
```

`x` ist 50 und z ist ein Pointer der auf den Speicherbereich  von x zeigt.