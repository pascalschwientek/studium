#include <stdio.h>
#include <stdlib.h>

#define ELEMENTS 6

int compare (const void * a, const void * b)
{
  return (*(int*)a - *(int*)b);
}

int main(void)
{
    int array[ELEMENTS], i;
    float median;

    printf("Programm Median\n");

    for(i=0; i<ELEMENTS; i++) {
    	printf("Bitte geben Sie den %d. Wert ein: ", i+1);
        scanf("%d", &array[i]);
    }

    qsort(array, ELEMENTS, sizeof(int), compare);

    if(ELEMENTS%2 == 0)
    {
        median = array[(ELEMENTS/2)-1] + array[ELEMENTS/2];
	median = median / 2;
    } else {
        median = array[ELEMENTS/2];
    }

    printf("Der Median der eingegebenen Zahlenfolge ist: %.1f\n",
        median
    );

    return 0;
}
