package de.plsk.factorysimulation.ui;

import de.plsk.factorysimulation.Machine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MachineSelectionPanel extends JPanel {
    private PanelManager panelManager;

    public MachineSelectionPanel(PanelManager panelManager) {
        super();
        this.panelManager = panelManager;
    }

    public void updateData() {
        this.removeAll();
        this.repaint();

        JPanel outerPanel = new JPanel();
        outerPanel.setLayout(new BoxLayout(outerPanel, BoxLayout.Y_AXIS));


        Iterator<Machine> iterator = panelManager.getActiveMachines().iterator();
        int index = 0;

        while(iterator.hasNext()) {
            Machine machine = iterator.next();

            JPanel itemPanel = this.makeItemPanel(machine, index);

            outerPanel.add(itemPanel);

            index++;
        }

        this.add(outerPanel, BorderLayout.NORTH);
        this.revalidate();
    }

    private JPanel makeItemPanel(Machine machine, int index) {
        JPanel itemPanel = new JPanel();
        itemPanel.setLayout(new BoxLayout(itemPanel, BoxLayout.Y_AXIS));
        itemPanel.setBackground(Color.lightGray);
        itemPanel.setBorder(BorderFactory.createLineBorder(Color.black));

        JLabel machineDataType = new JLabel("Type:   " + machine.getType());
        JLabel machineDataName = new JLabel("Name:   " + machine.getName());
        JLabel machineDataCost = new JLabel("Kosten: " + machine.getCost());
        itemPanel.add(machineDataType);
        itemPanel.add(machineDataName);
        itemPanel.add(machineDataCost);

        itemPanel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                panelManager.removeMachineFromFactory(index);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                itemPanel.setBorder(BorderFactory.createLineBorder(Color.red));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                itemPanel.setBorder(null);
            }
        });

        return itemPanel;
    }
}
