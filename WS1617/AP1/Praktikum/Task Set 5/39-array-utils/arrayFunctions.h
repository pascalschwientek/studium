#ifndef ARRAYFUNCTIONS_H
#define ARRAYFUNCTIONS_H

float mean(int numbers[], int size);
int biggest(int numbers[], int size);
int smallest(int numbers[], int size);
int secondSmallest(int numbers[], int size);

#endif /* ARRAYFUNCTIONS_H */