import java.util.Arrays;
import java.util.Scanner;

public class main
{
    public final static int ELEMENTS = 6;

    public final static Scanner STDIN_SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = new int[ELEMENTS];
        float median;

        System.out.println("Programm Median");

        for(int i = 0; i < ELEMENTS; i++) {
            System.out.print("Bitte geben Sie den " + (i + 1) + ". Wert ein: ");
            array[i] = STDIN_SCANNER.nextInt();
        }

        Arrays.sort(array, 0, ELEMENTS);

        if(ELEMENTS % 2 == 0) {
            median = array[ELEMENTS / 2 - 1] + array[ELEMENTS / 2];
            median = median / 2;
        } else {
            median = array[ELEMENTS / 2];
        }

        System.out.printf("Der Median der eingegebenen Zahlenfolge ist: %.1f\n", median);
    }
}